﻿using FluentValidation;
using Grpc.Core;
using Grpc.Core.Interceptors;
using SharedApi.Helpers;

namespace SharedApi.Interceptors
{
    public class ValidationInterceptor : Interceptor
    {
        public override Task<TResponse> UnaryServerHandler<TRequest, TResponse>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TResponse> continuation)
        {
            //Get validator service
            var validatorService = context.GetHttpContext().RequestServices.GetService(typeof(AbstractValidator<TRequest>)) as AbstractValidator<TRequest>;
            
            if (validatorService != null)
            {
                //Validate request
                var validationResult = validatorService.Validate(request);

                if (!validationResult.IsValid)
                {
                    string message = validationResult.ToString(", ");
                    var response = CommonHelper.GetFailResponse<TResponse>(message);
                    return Task.FromResult(response);
                }
            }

            return continuation(request, context);
        }
    }
}
