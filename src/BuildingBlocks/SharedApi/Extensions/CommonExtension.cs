﻿using Google.Protobuf.WellKnownTypes;

namespace SharedApi.Extensions
{
    public static class CommonExtension
    {
        public static Timestamp? GetTimestamp(this DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                return null;
            }

            return Timestamp.FromDateTime(dateTime.Value);
        }

        public static string ToGuidString(this Guid guid)
        {
            return guid == Guid.Empty ? string.Empty : guid.ToString();
        }
    }
}
