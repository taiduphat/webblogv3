﻿using System.Text.RegularExpressions;

namespace SharedApi.Helpers
{
    public static class CommonHelper
    {
        public static readonly string SlugRegex = "^[a-z0-9-]+$";
        public static bool IsMatchedRegex(string value)
        {
            return Regex.Match(value, SlugRegex, RegexOptions.IgnoreCase).Success;
        }
        public static bool IsValidGuid(string guid)
        {
            return Guid.TryParse(guid, out var result) && !result.Equals(Guid.Empty);
        }

        public static Guid? ParseGuid(string guid)
        {
            return !string.IsNullOrEmpty(guid) ? (Guid?)Guid.Parse(guid) : null;
        }

        public static bool IsEmptyOrValidGuid(string guid)
        {
            return string.IsNullOrEmpty(guid) || IsValidGuid(guid);
        }

        public static TResponse GetResponse<TResponse>(bool isSuccess, string message)
        {
            var responseObject = Activator.CreateInstance<TResponse>();
            var responseType = typeof(TResponse);
            // Set Status Code
            var statusProperty = responseType.GetProperty("Status");
            if (statusProperty != null)
            {
                var statusPropertyType = statusProperty.PropertyType;
                var statusPropertyObject = Activator.CreateInstance(statusPropertyType);

                statusPropertyType.GetProperty("IsSuccess")?.SetValue(statusPropertyObject, isSuccess);
                statusPropertyType.GetProperty("Message")?.SetValue(statusPropertyObject, message);

                responseType.GetProperty("Status")?.SetValue(responseObject, statusPropertyObject);
            }

            return responseObject;
        }

        public static TResponse GetFailResponse<TResponse>(string message)
        {
            return GetResponse<TResponse>(false, message);
        }

        public static string ToGuidString(this Guid guid)
        {
            return guid == Guid.Empty ? string.Empty : guid.ToString();
        }
    }
}
