﻿using Microsoft.EntityFrameworkCore;
using SharedCore.Interfaces;
using System.Linq.Expressions;

namespace SharedInfrastructure.Services
{
    public class Repository<TDbContext> : IRepository where TDbContext : DbContext
    {
        private readonly TDbContext _dbContext;

        public Repository(TDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task CreateAsync<TEntity>(TEntity entity) 
            where TEntity : class
        {
            _dbContext.Set<TEntity>().Add(entity);
            await Task.CompletedTask;
        }

        public async Task CreateRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            foreach (var item in entities)
            {
                await CreateAsync<TEntity>(item);
            }
            await Task.CompletedTask;
        }

        public async Task DeleteAsync<TEntity>(TEntity entity) 
            where TEntity : class
        {
            _dbContext.Set<TEntity>().Remove(entity);
            await Task.CompletedTask;
        }

        public async Task DeleteRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            foreach (var item in entities)
            {
                await DeleteAsync<TEntity>(item);
            }
            await Task.CompletedTask;
        }

        public void EnsureDatabaseCreated()
        {
            _dbContext.Database.EnsureCreated();
        }

        public void EnsureDatabaseDeleted()
        {
            _dbContext.Database.EnsureDeleted();
        }

        public async Task<List<TEntity>> FindAsync<TEntity>(Expression<Func<TEntity, bool>> expression) 
            where TEntity : class
        {
            var entities = await _dbContext.Set<TEntity>()
                .Where(expression)
                .ToListAsync();

            return entities;
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<List<TEntity>> SelectAllAsync<TEntity>() 
            where TEntity : class
        {
            return await _dbContext.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity?> SelectByIdAsync<TEntity>(Guid id) 
            where TEntity : class
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task UpdateAsync<TEntity>(TEntity entity) 
            where TEntity : class
        {
            _dbContext.Set<TEntity>().Update(entity);
            await Task.CompletedTask;
        }
    }
}
