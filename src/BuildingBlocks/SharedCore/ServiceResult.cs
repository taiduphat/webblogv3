﻿namespace SharedCore
{
    public class ServiceResult<T>
    {
        public StatusCode StatusCode { get; set; }
        public T Result { get; set; }
        public bool IsSuccess => StatusCode == StatusCode.Success;
        protected ServiceResult() { }
        public static ServiceResult<T> Success(T result)
        {
            return new ServiceResult<T>
            {
                StatusCode = StatusCode.Success,
                Result = result
            };
        }
        public static ServiceResult<T> Fail(StatusCode statusCode)
        {
            return new ServiceResult<T>
            {
                StatusCode = statusCode
            };
        }
    }

}
