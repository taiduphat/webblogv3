﻿namespace SharedCore.Config
{
    public class ApiConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Scheme { get; set; }
    }
}
