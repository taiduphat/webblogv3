﻿using System.Linq.Expressions;

namespace SharedCore.Interfaces
{
    public interface IRepository
    {
        Task<List<TEntity>> SelectAllAsync<TEntity>() where TEntity : class;
        Task<TEntity?> SelectByIdAsync<TEntity>(Guid id) where TEntity : class;
        Task<List<TEntity>> FindAsync<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class;
        Task CreateAsync<TEntity>(TEntity entity) where TEntity : class;
        Task CreateRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class;
        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class;
        Task DeleteRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
        Task<int> SaveChangesAsync();
        void EnsureDatabaseCreated();
        void EnsureDatabaseDeleted();
    }
}
