﻿using System.ComponentModel.DataAnnotations;

namespace SharedCore
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}