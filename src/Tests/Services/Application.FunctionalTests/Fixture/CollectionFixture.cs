﻿using Application.FunctionalTests.Helpers;
using Grpc.Net.Client;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;

namespace Application.FunctionalTests.Fixture
{
    public class CollectionFixture<TStartup> : IDisposable where TStartup : class
    {
        private readonly TestServer _testServer;
        
        public GrpcChannel Channel = default!;
        public IServiceProvider ServiceProvider => _testServer.Services;

        public CollectionFixture()
        {
            var startupType = typeof(TStartup);
            var webBuilder = WebHost.CreateDefaultBuilder()
                .UseWebRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostContext, configBuilder) => 
                {
                    configBuilder.Sources.Clear();
                    configBuilder.AddJsonFile($"appsettings.{startupType.Name}.json", true, false); 
                })
                .UseStartup<TStartup>();

            _testServer = new TestServer(webBuilder);
            var client = _testServer.CreateClient();

            DbContextHelper.EnsureCreated(_testServer.Services);

            Debug.WriteLine($"Start test server which {startupType.Name} startup successfully");

            var uri = client.BaseAddress ?? throw new ArgumentNullException(nameof(client));
            Channel = GrpcChannel.ForAddress(uri, new GrpcChannelOptions { HttpClient = client });
        }

        public void Dispose()
        {
            _testServer.Dispose();
            if (Channel != null)
            {
                Channel.Dispose();
            }
            GC.SuppressFinalize(this);


            Debug.WriteLine($"Dispose was called by CollectionFixture of {typeof(TStartup).Name}");
        }
    }
}
