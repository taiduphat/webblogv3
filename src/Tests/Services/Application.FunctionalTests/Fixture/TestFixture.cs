using Xunit;

namespace Application.FunctionalTests.Fixture
{
    public class TestFixture<TStartup, TGrpcClient>
        where TStartup : class
        where TGrpcClient : class
    {
        public TGrpcClient Client;
        public IServiceProvider ServiceProvider;

        public TestFixture(CollectionFixture<TStartup> collectionFixture)
        {
            var clientInstance = Activator.CreateInstance(typeof(TGrpcClient), collectionFixture.Channel);
            if (clientInstance != null)
                Client = (TGrpcClient)clientInstance;
            
            ArgumentNullException.ThrowIfNull(Client);

            ServiceProvider = collectionFixture.ServiceProvider;
        }
    }
}
