﻿using Application.FunctionalTests.Fixture;
using Application.FunctionalTests.Scenarios.Item;
using Application.FunctionalTests.Scenarios.Template;
using Xunit;

namespace Api.IntegrationTest.Fixture
{
    [CollectionDefinition(TestConstants.TemplateCollectionFixture)]
    public class TemplateCollectionFixtureDefinition : ICollectionFixture<CollectionFixture<TemplateTestStartup>>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }

    [CollectionDefinition(TestConstants.ItemCollectionFixture)]
    public class ItemCollectionFixtureDefinition : ICollectionFixture<CollectionFixture<ItemTestStartup>>
    {
    }
}
