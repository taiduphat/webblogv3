﻿namespace Application.FunctionalTests.Fixture
{
    public static class TestConstants
    {
        public const string AuthenticationScheme = "TestSchema";
        public const string TemplateCollectionFixture = "TemplateCollectionFixture";
        public const string ItemCollectionFixture = "ItemCollectionFixture";
    }
}
