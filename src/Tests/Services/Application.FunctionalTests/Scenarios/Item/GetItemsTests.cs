﻿using Application.FunctionalTests.Fixture;
using Application.FunctionalTests.Helpers;
using ItemApi;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Application.FunctionalTests.Scenarios.Item
{
    [Collection(TestConstants.ItemCollectionFixture)]
    public class GetItemsTests : IClassFixture<TestFixture<ItemTestStartup, ItemApi.Item.ItemClient>>
    {
        private readonly ItemApi.Item.ItemClient _client;
        private readonly IServiceProvider _serviceProvider;

        public GetItemsTests(TestFixture<ItemTestStartup, ItemApi.Item.ItemClient> testFixture)
        {
            _client = testFixture.Client;
            _serviceProvider = testFixture.ServiceProvider;
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        public async Task GetItemById_WithInvalidId_Fail(string itemId)
        {
            // Arrange 
            var request = new GetItemByIdRequest
            {
                ItemId = itemId
            };

            // Act
            var result = await _client.GetItemByIdAsync(request);

            // Assert
            Assert.NotNull(result);
            Assert.True(!result.Status.IsSuccess && result.Item == null);
        }

        [Fact]
        public async Task GetItemById_WithValidData_Success()
        {
            string templateId = Guid.NewGuid().ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                Id = Guid.NewGuid().ToString(),
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });

            var fieldDtos = prepareItemRequest.ItemValues.Select(x => new ItemService.Core.Dtos.FieldDto
            {
                Id = Guid.Parse(x.FieldId),
                FieldTypeId = Guid.NewGuid()
            });
            var templateGatewayMock = _serviceProvider.GetService<ItemService.Core.Gateway.ITemplateGateway>();
            Mock.Get(templateGatewayMock)
                .Setup(_ => _.GetFieldsByTemplateId(Guid.Parse(templateId)))
                .ReturnsAsync(fieldDtos);

            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemRequest = new GetItemByIdRequest
            {
                ItemId = prepareItemResponse.ItemId
            };
            // Act
            var result = await _client.GetItemByIdAsync(getItemRequest);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status.IsSuccess);
            Assert.True(result.Item.Id == getItemRequest.ItemId);
        }

        [Fact]
        public async Task GetItems_WithValidData_Success()
        {
            string templateId = Guid.NewGuid().ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });
            
            //Simulate the gateway response
            var fieldDtos = prepareItemRequest.ItemValues.Select(x => new ItemService.Core.Dtos.FieldDto
            {
                Id = Guid.Parse(x.FieldId),
                FieldTypeId = Guid.NewGuid()
            });
            var templateGatewayMock = _serviceProvider.GetService<ItemService.Core.Gateway.ITemplateGateway>();
            Mock.Get(templateGatewayMock)
                .Setup(_ => _.GetFieldsByTemplateId(Guid.Parse(templateId)))
                .ReturnsAsync(fieldDtos);

            await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemRequest = new GetItemsRequest();
            // Act
            var result = await _client.GetItemsAsync(getItemRequest);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status.IsSuccess);
            Assert.True(result.Items.Any());
        }
    }
}
