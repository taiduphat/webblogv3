﻿using Application.FunctionalTests.Fixture;
using ItemService.Api;
using ItemService.Core.Gateway;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Moq;

namespace Application.FunctionalTests.Scenarios.Item
{
    public class ItemTestStartup : Startup
    {
        public ItemTestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(TestConstants.AuthenticationScheme)
                .AddScheme<AuthenticationSchemeOptions, TestAuthenticationHandler>(TestConstants.AuthenticationScheme, null);

            services.AddAuthorization();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            // Mock external api integration
            services.Replace(ServiceDescriptor.Singleton(x => Mock.Of<ITemplateGateway>()));
        }
    }
}
