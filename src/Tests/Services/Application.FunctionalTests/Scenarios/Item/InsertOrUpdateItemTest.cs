﻿using Application.FunctionalTests.Fixture;
using Application.FunctionalTests.Helpers;
using ItemApi;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;

namespace Application.FunctionalTests.Scenarios.Item
{
    [Collection(TestConstants.ItemCollectionFixture)]
    public class InsertOrUpdateItemTest : IClassFixture<TestFixture<ItemTestStartup, ItemApi.Item.ItemClient>>
    {
        private readonly ItemApi.Item.ItemClient _client;
        private readonly IServiceProvider _serviceProvider;

        public InsertOrUpdateItemTest(TestFixture<ItemTestStartup, ItemApi.Item.ItemClient> testFixture)
        {
            _client = testFixture.Client;
            _serviceProvider = testFixture.ServiceProvider;
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        [InlineData("a2bdd9ca-ea15")]
        public async Task InsertOrUpdateItem_WhichInvalidTemplateId_Fail(string templateId)
        {
            var request = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        [InlineData("a2bdd9ca-ea15")]
        public async Task InsertOrUpdateItem_WhichInvalidFieldId_Fail(string fieldId)
        {
            var itemValues = new List<ItemValueRequest>
            {
                new ItemValueRequest
                {
                    FieldId = fieldId,
                    Value = $"Value_{Guid.NewGuid()}"
                }
            };
            var request = PrepareDataHelper.GenerateItemRequest(string.Empty, Guid.NewGuid().ToString(), itemValues);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task InsertOrUpdateItem_WhichInvalidItemId_Fail()
        {
            string invalidId = Guid.NewGuid().ToString().Substring(0, 10);
            var request = PrepareDataHelper.GenerateItemRequest(invalidId, Guid.NewGuid().ToString(), new List<ItemValueRequest>());

            // Act
            var response = await _client.InsertOrUpdateItemAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task InsertItem_WhichValidData_Success()
        {
            // Prepare template
            string templateId = Guid.NewGuid().ToString();

            var itemValues = new List<ItemValueRequest>();
            foreach (var value in Enumerable.Range(0, 5).Select(x => Guid.NewGuid()))
            {
                itemValues.Add(new ItemValueRequest
                {
                    FieldId = value.ToString(),
                    Value = $"Value_{Guid.NewGuid()}"
                });
            }
            
            var insertItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, itemValues);

            SimulateTemplateGatewayIntegration(insertItemRequest);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));
        }

        [Fact]
        public async Task UpdateItem_BothAddsAndUpdatesNewField_Success()
        {
            string templateId = Guid.NewGuid().ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });
            SimulateTemplateGatewayIntegration(prepareItemRequest);
            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            var getItemResponse = await _client.GetItemByIdAsync(new GetItemByIdRequest { ItemId = prepareItemResponse.ItemId });
            var item = getItemResponse.Item;

            var insertItemRequest = new InsertOrUpdateItemRequest
            {
                Id = item.Id,
                Name = item.Name,
                Slug = item.Slug,
                TemplateId = item.TemplateId,
                ParentItemId = item.ParentItemId,
                ItemValues = {
                    item.ItemValues.Select(x => new ItemValueRequest
                    {
                        Id = x.Id,
                        FieldId = x.FieldId,
                        Value = x.Value
                    })
                }
            };

            // Update new value of old field
            insertItemRequest.ItemValues[0].Value = Guid.Empty.ToString();
            // Add new field
            insertItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });
            SimulateTemplateGatewayIntegration(insertItemRequest);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));

            var getItemRequest = new GetItemByIdRequest { ItemId = response.ItemId };
            var itemValues = (await _client.GetItemByIdAsync(getItemRequest)).Item.ItemValues;

            var result = from x in insertItemRequest.ItemValues
                         join y in itemValues on new { x.FieldId, x.Value } equals new { y.FieldId, y.Value }
                         select new { y.Id, y.FieldId, y.Value };
            Assert.True(result.Count() == itemValues.Count);
        }

        [Fact]
        public async Task InsertItem_WhichValidParentItem_Success()
        {
            string templateId = Guid.NewGuid().ToString();

            var prepareItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, new List<ItemValueRequest>());
            prepareItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });
            var insertItemRequest = prepareItemRequest.Clone();
            SimulateTemplateGatewayIntegration(insertItemRequest);
            var prepareItemResponse = await _client.InsertOrUpdateItemAsync(prepareItemRequest);

            
            insertItemRequest.ParentItemId = prepareItemResponse.ItemId;
            // Update new value of old field
            insertItemRequest.ItemValues[0].Value = Guid.Empty.ToString();
            // Add new field
            insertItemRequest.ItemValues.Add(new ItemValueRequest
            {
                FieldId = Guid.NewGuid().ToString(),
                Value = $"Value_{Guid.NewGuid()}"
            });
            SimulateTemplateGatewayIntegration(insertItemRequest);

            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.ItemId));

            var getItemRequest = new GetItemByIdRequest { ItemId = response.ItemId };
            var item = (await _client.GetItemByIdAsync(getItemRequest)).Item;

            Assert.True(item.ParentItemId == prepareItemResponse.ItemId);
        }

        [Fact]
        public async Task InsertItem_WhichInvalidParentItem_Fail()
        {
            string templateId = Guid.NewGuid().ToString();

            var itemValues = new List<ItemValueRequest>();
            foreach (var fieldId in Enumerable.Range(0, 6).Select(_ => Guid.NewGuid().ToString()))
            {
                itemValues.Add(new ItemValueRequest
                {
                    FieldId = fieldId,
                    Value = $"Value_{Guid.NewGuid()}"
                });
            }
            var insertItemRequest = PrepareDataHelper.GenerateItemRequest(string.Empty, templateId, itemValues);
            SimulateTemplateGatewayIntegration(insertItemRequest);

            insertItemRequest.ParentItemId = Guid.NewGuid().ToString();
            // Act
            var response = await _client.InsertOrUpdateItemAsync(insertItemRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess);
        }

        protected void SimulateTemplateGatewayIntegration(InsertOrUpdateItemRequest insertItemRequest)
        {
            var fieldDtos = insertItemRequest.ItemValues.Select(x => new ItemService.Core.Dtos.FieldDto
            {
                Id = Guid.Parse(x.FieldId),
                FieldTypeId = Guid.NewGuid()
            });
            var templateGatewayMock = _serviceProvider.GetService<ItemService.Core.Gateway.ITemplateGateway>();
            Mock.Get(templateGatewayMock)
                .Setup(_ => _.GetFieldsByTemplateId(Guid.Parse(insertItemRequest.TemplateId)))
                .ReturnsAsync(fieldDtos);
        }
    }
}
