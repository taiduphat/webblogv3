﻿using Application.FunctionalTests.Fixture;
using Application.FunctionalTests.Helpers;
using TemplateApi;
using TemplateService.Core;
using Xunit;

namespace Application.FunctionalTests.Scenarios.Template
{
    [Collection(TestConstants.TemplateCollectionFixture)]
    public class InsertOrUpdateTemplateTest : IClassFixture<TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient>>
    {
        private readonly TemplateApi.Template.TemplateClient _client;

        public InsertOrUpdateTemplateTest(TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient> testFixture)
        {
            _client = testFixture.Client;
        }

        [Fact]
        public async Task InsertOrUpdateTemplate_WhichInvalidTemplateId_Fail()
        {
            var request = PrepareDataHelper.GenerateTemplateRequest();
            request.Id = Guid.NewGuid().ToString().Remove(0, 10);

            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.TemplateId));
        }

        [Fact]
        public async Task InsertOrUpdateTemplate_WhichInvalidFieldId_Fail()
        {
            var request = PrepareDataHelper.GenerateTemplateRequest();
            request.Fields[1].Id = Guid.NewGuid().ToString().Remove(0, 10);

            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.TemplateId));
        }

        [Theory]
        [InlineData("")]
        [InlineData("a2bdd9ca-ea15-46e4-aa4e-8ffac9de3c4b")]
        public async Task InsertOrUpdateTemplate_WhichInvalidFieldTypeId_Fail(string fieldTypeId)
        {
            var request = PrepareDataHelper.GenerateTemplateRequest();
            request.Fields[1].FieldTypeId = fieldTypeId;

            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.True(!response.Status.IsSuccess && string.IsNullOrEmpty(response.TemplateId));
        }

        [Fact]
        public async Task InsertTemplate_WhichValidData_Success()
        {
            var insertTemplateRequest = PrepareDataHelper.GenerateTemplateRequest();

            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(insertTemplateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.TemplateId));
        }

        [Fact]
        public async Task UpdateTemplate_WhichAddingNewField_Success()
        {
            var insertResp = await _client.InsertOrUpdateTemplateAsync(PrepareDataHelper.GenerateTemplateRequest());
            Assert.True(insertResp != null && insertResp.Status.IsSuccess);

            var updateRequest = PrepareDataHelper.GenerateTemplateRequest();
            updateRequest.Id = insertResp.TemplateId;
            updateRequest.Fields.Clear();

            var fieldRequest = new FieldRequest
            {
                Id = string.Empty,
                FieldTypeId = Constants.BoolId.ToString(),
                Name = "New field"
            };
            updateRequest.Fields.Add(fieldRequest);
            
            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(updateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.TemplateId));

            var getFieldsRequest = new GetTemplateByIdRequest { TemplateId = response.TemplateId };
            var fields = (await _client.GetTemplateByIdAsync(getFieldsRequest)).Template.Fields;
            Assert.True(fields?.Any(x => x.Name.Equals(fieldRequest.Name) && x.FieldTypeId == fieldRequest.FieldTypeId));
        }

        [Fact]
        public async Task UpdateTemplate_WhichExistingField_Success()
        {
            // Prepare template in database
            var insertRequest = PrepareDataHelper.GenerateTemplateRequest();
            var insertResp = await _client.InsertOrUpdateTemplateAsync(insertRequest);
            Assert.True(insertResp != null && insertResp.Status.IsSuccess);

            var getTemplateResponse = await _client.GetTemplateByIdAsync(new GetTemplateByIdRequest { TemplateId = insertResp.TemplateId });
            var template = getTemplateResponse.Template;

            var fieldRequest = new FieldRequest
            {
                Id = template.Fields.First().Id,
                FieldTypeId = Constants.BoolId.ToString(),
                Name = "New field"
            };

            var updateRequest = new InsertOrUpdateTemplateRequest
            {
                Id = template.Id,
                Name = template.Name,
                Slug = template.Slug,
                Fields = { fieldRequest }
            };

            // Act
            var response = await _client.InsertOrUpdateTemplateAsync(updateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.TemplateId));

            var getFieldsRequest = new GetTemplateByIdRequest { TemplateId = response.TemplateId };
            var fields = (await _client.GetTemplateByIdAsync(getFieldsRequest)).Template.Fields;
            Assert.True(fields?.Any(x => x.Id == fieldRequest.Id && x.Name.Equals(fieldRequest.Name) && x.FieldTypeId == fieldRequest.FieldTypeId));
        }
    }
}
