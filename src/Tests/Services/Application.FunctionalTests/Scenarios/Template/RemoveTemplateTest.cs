﻿using Application.FunctionalTests.Fixture;
using Application.FunctionalTests.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using TemplateApi;
using Xunit;

namespace Application.FunctionalTests.Scenarios.Template
{
    [Collection(TestConstants.TemplateCollectionFixture)]
    public class RemoveTemplateTest : IClassFixture<TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient>>
    {
        private readonly TemplateApi.Template.TemplateClient _client;

        public RemoveTemplateTest(TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient> testFixture)
        {
            _client = testFixture.Client;
        }

        [Fact]
        public async Task RemoveTemplate_WhichInvalidTemplateId_Fail()
        {
            var request = new RemoveTemplateRequest { TemplateId = Guid.NewGuid().ToString().Remove(0, 10) };

            // Act
            var response = await _client.RemoveTemplateAsync(request);

            // Assert
            Assert.NotNull(response);
            Assert.False(response.Status.IsSuccess);
        }
    }
}
