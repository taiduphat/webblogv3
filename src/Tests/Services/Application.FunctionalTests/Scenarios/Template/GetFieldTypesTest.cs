﻿using Application.FunctionalTests.Fixture;
using Xunit;

namespace Application.FunctionalTests.Scenarios.Template
{
    [Collection(TestConstants.TemplateCollectionFixture)]
    
    public class GetFieldTypesTest : IClassFixture<TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient>>
    {
        private readonly TemplateApi.Template.TemplateClient _client;

        public GetFieldTypesTest(TestFixture<TemplateTestStartup, TemplateApi.Template.TemplateClient> testFixture)
        {
            _client = testFixture.Client;
        }

        [Fact]
        public async Task GetFieldTypes_ShouldRunSuccess()
        {
            // Act
            var result = await _client.GetFieldTypesAsync(new Google.Protobuf.WellKnownTypes.Empty());

            // Assert
            Assert.True(result != null && result.Status.IsSuccess && result.FieldTypes.Any());
        }
    }
}
