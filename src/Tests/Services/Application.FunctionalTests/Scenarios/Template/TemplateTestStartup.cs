﻿using Application.FunctionalTests.Fixture;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TemplateService.Api;

namespace Application.FunctionalTests.Scenarios.Template
{
    public class TemplateTestStartup : Startup
    {
        public TemplateTestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        protected override void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(TestConstants.AuthenticationScheme)
                .AddScheme<AuthenticationSchemeOptions, TestAuthenticationHandler>(TestConstants.AuthenticationScheme, null);

            services.AddAuthorization();
        }
    }
}
