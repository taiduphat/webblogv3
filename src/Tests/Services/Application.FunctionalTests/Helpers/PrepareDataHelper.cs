﻿using Fare;
using ItemApi;
using SharedApi.Helpers;
using TemplateApi;
using TemplateService.Core;
using Xunit;

namespace Application.FunctionalTests.Helpers
{
    public class PrepareDataHelper
    {
        private static readonly Xeger _xeger = new(CommonHelper.SlugRegex, new Random());

        public static InsertOrUpdateTemplateRequest GenerateTemplateRequest()
        {
            var templateRequest = new InsertOrUpdateTemplateRequest
            {
                Name = $"Name_{Guid.NewGuid()}",
                Slug = _xeger.Generate(),
                Fields = 
                {
                    new FieldRequest
                    {
                        Name = $"FieldName_{Guid.NewGuid()}",
                        FieldTypeId = Constants.PlainTextId.ToString(),
                    },
                    new FieldRequest
                    {
                        Name = $"FieldName_{Guid.NewGuid()}",
                        FieldTypeId = Constants.RichTextId.ToString(),
                    }
                }
            };

            return templateRequest;
        }

        public static async Task<TemplateResponse> AddingTemplate(Template.TemplateClient client)
        {
            var insertTemplateRequest = GenerateTemplateRequest();

            // Act
            var response = await client.InsertOrUpdateTemplateAsync(insertTemplateRequest);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.Status.IsSuccess && !string.IsNullOrEmpty(response.TemplateId));

            var getTemplateResponse = await client.GetTemplateByIdAsync(new GetTemplateByIdRequest { TemplateId = response.TemplateId });

            return getTemplateResponse.Template;
        }

        public static InsertOrUpdateItemRequest GenerateItemRequest(string itemId, string templateId, List<ItemValueRequest> itemValues)
        {
            var itemRequest = new InsertOrUpdateItemRequest
            {
                Id = itemId,
                Name = $"Name_{Guid.NewGuid()}",
                Slug = _xeger.Generate(),
                TemplateId = templateId,
                ItemValues = { itemValues }
            };

            return itemRequest;
        }
    }
}
