﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SharedCore;
using SharedCore.Interfaces;

namespace Application.FunctionalTests.Helpers
{
    public class DbContextHelper
    {
        public static void EnsureCreated(IServiceProvider serviceProvider)
        {
            // Create a scope to obtain a reference to the database
            using var scope = serviceProvider.CreateScope();
            var scopedServices = scope.ServiceProvider;

            var repository = scopedServices.GetService<IRepository>();
            // Ensure the database is created.
            repository?.EnsureDatabaseCreated();
        }

        public static void EnsureDeleted(IServiceProvider serviceProvider)
        {
            // Create a scope to obtain a reference to the database
            using var scope = serviceProvider.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var repository = scopedServices.GetService<IRepository>();

            // Ensure the database is deleteed.
            repository?.EnsureDatabaseDeleted();
        }
    }
}
