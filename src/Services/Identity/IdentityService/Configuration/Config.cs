﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using static IdentityService.IdentityServiceContants;

namespace IdentityService.Configuration
{
    public class Config
    {
        // Identity resources are data like user ID, name, or email address of a user
        // see: http://docs.identityserver.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),

                // The identity resource itself has no these claims, we need to add them
                new IdentityResource("roles", "User roles", new[] { JwtClaimTypes.Role, ClaimType.Permission })
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new[]
            {
                // Adding the permission claim to access token
                new ApiResource("webblogapi")
                {
                    Scopes = new[]
                    {
                        "apiread",
                        "apiwrite"
                    },
                    UserClaims = new[] { ClaimType.Permission }
                }
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new[]
            {
                new ApiScope("apiread"), new ApiScope("apiwrite")
            };
        }

        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new[]
            {
                new Client
                {
                    ClientId = "interactive",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RedirectUris = Array.Empty<string>(),
                    AllowedCorsOrigins = Array.Empty<string>(),
                    PostLogoutRedirectUris = Array.Empty<string>(),
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "apiread",
                        "apiwrite",
                        "roles"
                    },
                }

            };
        }
    }
}
