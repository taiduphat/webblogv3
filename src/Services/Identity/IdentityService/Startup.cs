using IdentityModel;
using IdentityService.Configuration;
using IdentityService.Persistence.IdentityDbContext;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;

namespace IdentityService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            
            services.AddDbContext<AppIdentityDbContext>(options => 
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>()  
                .AddDefaultTokenProviders();

            services.AddIdentityServer()
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddInMemoryApiScopes(Config.GetApiScopes())
                .AddInMemoryClients(Config.GetClients(Configuration.GetValue<Dictionary<string, string>>("IdentityServer:Clients")))
                .AddDeveloperSigningCredential()
                .AddAspNetIdentity<IdentityUser>();


            services.AddAuthentication();
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);

                options.LoginPath = "/Account/Login";
                options.SlidingExpiration = true;
            });
            
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
				IdentityModelEventSource.ShowPII = true;
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseIdentityServer();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            CreateUserRoles(serviceProvider).Wait();
        }


        private async Task CreateUserRoles(IServiceProvider appServices)
        {
            var roleManager = appServices.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = appServices.GetRequiredService<UserManager<IdentityUser>>();
            
            // Adding admin role
            bool roleCheck = await roleManager.RoleExistsAsync(IdentityServiceContants.Role.Administrator);
            if (!roleCheck)
            {
                await roleManager.CreateAsync(new IdentityRole(IdentityServiceContants.Role.Administrator));
            }
            
            // Adding editor role
            roleCheck = await roleManager.RoleExistsAsync(IdentityServiceContants.Role.User);
            if (!roleCheck)
            {
                await roleManager.CreateAsync(new IdentityRole(IdentityServiceContants.Role.User));
            }
            
            // Adding user admin
            string fullName = Configuration.GetSection("UserSettings")["FullName"];
            string userName = Configuration.GetSection("UserSettings")["Username"];
            string pass = Configuration.GetSection("UserSettings")["UserPassword"];
            
            var powerUser = new IdentityUser(userName);

            var user = await userManager.FindByNameAsync(powerUser.UserName);
            if (user == null)
            {
                var identityResult = await userManager.CreateAsync(powerUser, pass);
                if (identityResult.Succeeded)
                {
                    //here we tie the new user to the "Admin" role 
                    await userManager.AddToRoleAsync(powerUser, IdentityServiceContants.Role.Administrator);
                    await userManager.AddClaimAsync(powerUser, new System.Security.Claims.Claim(JwtClaimTypes.Name, fullName));
                }
            }
        }
    }
}
