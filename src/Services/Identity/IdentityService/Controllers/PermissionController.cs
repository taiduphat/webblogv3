﻿using IdentityService.Helpers;
using IdentityService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityService.Controllers
{
    [Authorize(Roles = $"{IdentityServiceContants.Role.Administrator}")]
    public class PermissionController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public PermissionController(RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<ActionResult> Index(string roleId)
        {
            var model = new PermissionViewModel();

            var role = await _roleManager.FindByIdAsync(roleId);
            model.RoleId = roleId;

            var roleClaims = await _roleManager.GetClaimsAsync(role);
            var roleClaimValues = roleClaims.Select(a => a.Value).ToList();
            
            model.RoleClaims = roleClaimValues;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddPermission(string roleId, string permissionName)
        {
            if (permissionName != null)
            {
                var role = await _roleManager.FindByIdAsync(roleId);

                await _roleManager.AddClaimAsync(role, ClaimsHelper.CreatePermissionClaim(permissionName));
            }
            return RedirectToAction("Index", "Permission", new { roleId });
        }

        public async Task<IActionResult> DeletePermission(string roleId, string permissionName)
        {
            if (permissionName != null)
            {
                var role = await _roleManager.FindByIdAsync(roleId);

                await _roleManager.RemoveClaimAsync(role, ClaimsHelper.CreatePermissionClaim(permissionName));
            }
            return RedirectToAction("Index", "Permission", new { roleId });
        }
    }
}
