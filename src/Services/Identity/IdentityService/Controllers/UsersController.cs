﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityService.Models;
using IdentityModel;

namespace IdentityService.Controllers
{
    [Authorize(Roles = $"{IdentityServiceContants.Role.Administrator}")]
    public class UsersController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UsersController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            var identityUsers = await _userManager.Users.ToListAsync();

            List<UserListViewModel.UserItemModel> userList = new();
            foreach (var user in identityUsers)
            {
                var userClaims = await _userManager.GetClaimsAsync(user);
                userList.Add(CreateUserItemModel(user, userClaims));
            }

            return View(new UserListViewModel { Users = userList });
        }

        private UserListViewModel.UserItemModel CreateUserItemModel(IdentityUser identityUser, IList<Claim> userClaims)
        {
            var createdAtClaim = userClaims.FirstOrDefault(x => x.Type == IdentityServiceContants.ClaimType.CreatedAt)?.Value;
            DateTime.TryParse(createdAtClaim, out var createdAt);

            var fullName = userClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name)?.Value;

            return new UserListViewModel.UserItemModel
            {
                FullName = fullName,
                UserName = identityUser.UserName,
                UserId = identityUser.Id,
                CreatedAt = createdAt
            };
        }

        public async Task<IActionResult> Update(string userId)
        {
            var userIdentity = await _userManager.FindByIdAsync(userId);
            var userClaims = await _userManager.GetClaimsAsync(userIdentity);
            var userRoles = await _userManager.GetRolesAsync(userIdentity);
            
            var selectableRoles = await _roleManager.Roles.ToListAsync();
            var userInfo = CreateUserItemModel(userIdentity, userClaims);
            
            var userDetailViewModel = new UserDetailViewModel
            {
                UserId = userInfo.UserId,
                FullName = userInfo.FullName,
                UserName = userInfo.UserName,
                CreatedAt = userInfo.CreatedAt,
            };

            userDetailViewModel.RoleIds = userRoles;
            userDetailViewModel.SelectableRoleList = selectableRoles.Select(x => x.Name).ToList();

            return View(userDetailViewModel);
        }
    }
}
