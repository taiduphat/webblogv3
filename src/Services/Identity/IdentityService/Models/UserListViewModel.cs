﻿namespace IdentityService.Models
{
    public class UserListViewModel
    {
        public List<UserItemModel> Users { get; set; }

        public class UserItemModel
        {
            public string UserId { get; set; }
            public string UserName { get; set; }
            public string FullName { get; set; }
            public DateTime CreatedAt { get; set; }
        }
    }
}
