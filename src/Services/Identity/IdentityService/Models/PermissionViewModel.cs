﻿using System.Collections.Generic;

namespace IdentityService.Models
{
    public class PermissionViewModel
    {
        public string RoleId { get; set; }
        public IList<string> RoleClaims { get; set; }
    }
}
