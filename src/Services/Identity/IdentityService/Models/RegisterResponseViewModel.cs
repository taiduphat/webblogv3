﻿namespace IdentityService.Models
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
    }
}