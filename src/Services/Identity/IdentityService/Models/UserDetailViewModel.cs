﻿using System.ComponentModel.DataAnnotations;

namespace IdentityService.Models
{
    public class UserDetailViewModel
    {
        public string UserId { get; set; }

        [Display(Name = "User name")]
        public string UserName { get; set; }

        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        [Display(Name = "Created at")]
        public DateTime CreatedAt { get; set; }

        [Display(Name = "Roles")]
        public IList<string> RoleIds { get; set; }
        public IList<string> SelectableRoleList { get; set; }
    }
}
