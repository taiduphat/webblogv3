﻿using System.Security.Claims;

namespace IdentityService.Helpers
{
    public class ClaimsHelper
    {
        public static Claim CreatePermissionClaim(string permissionValue)
        {
            return new Claim(IdentityServiceContants.ClaimType.Permission, permissionValue.Trim());
        }
    }
}
