﻿using FileService.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedCore;

namespace FileService
{
    public static class DependencyInjection
    {
        public static void AddFileService(this IServiceCollection services, IConfiguration configuration)
        {
            // Core services
            services.AddScoped<IFileStorageService, Core.Services.FileStorageService>();
            services.AddScoped<IFileStorage, Infrastructure.Services.LocalFileStorage>();

            // DbContext 
            services.AddDbContext<Infrastructure.Persistence.FileDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(Infrastructure.Persistence.FileDbContext).Assembly.FullName)));
            
            services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<Infrastructure.Persistence.FileDbContext>());
        }
    }
}