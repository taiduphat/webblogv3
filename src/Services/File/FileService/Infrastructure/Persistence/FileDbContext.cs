﻿using FileService.Core.Entities;
using Microsoft.EntityFrameworkCore;
using SharedInfrastructure;

namespace FileService.Infrastructure.Persistence
{
    /// <summary>
    /// Implement of the IDesignTimeDbContextFactory 
    /// Supporting for generating the EF migration at design-time (EF code-first approach). Meaning that we could creating migration in this project as well.
    /// </summary>
    /// 'Add-Migration Initial_TemplateDb -o "Infrastructure\Persistence\Migrations" -c TemplateDbContext'
    public class FileDbContextDesignFactory : DbContextDesignFactoryBase<FileDbContext>
    {
    }
    public class FileDbContext : DbContext, Core.Interfaces.IFileDbContext
    {
        public DbSet<FileStorage> FileStorages { get; set; } = default!;

        public FileDbContext(DbContextOptions options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.InitialDateTimeConverter();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }
    }
}
