﻿using FileService.Core.Interfaces;
using Microsoft.Extensions.FileProviders;

namespace FileService.Infrastructure.Services
{
    public class LocalFileStorage : IFileStorage
    {
        private readonly PhysicalFileProvider _physicalFileProvider;

        public LocalFileStorage(IFileProvider fileProvider)
        {
            _physicalFileProvider = (PhysicalFileProvider)fileProvider;
            if (!Directory.Exists(_physicalFileProvider.Root))
            {
                Directory.CreateDirectory(_physicalFileProvider.Root);
            }
        }

        public Task<bool> RemoveFile(string filePath)
        {
            File.Delete(filePath);
            return Task.FromResult(true);
        }

        public async Task UploadFile(string filePath, byte[] content)
        {
            //Create the file.
            using (FileStream fileStream = File.Create(filePath))
            {
                await fileStream.WriteAsync(content).ConfigureAwait(false);
            }

            await Task.CompletedTask;
        }

        public string GetFilePath(string fileName) => _physicalFileProvider.GetFileInfo(fileName).PhysicalPath;

        public async Task<byte[]> GetFileContent(string filePath)
        {
            var fileInfo = _physicalFileProvider.GetFileInfo(filePath);
            var content = await File.ReadAllBytesAsync(fileInfo.PhysicalPath);
            
            return content;
        }
    }

}
