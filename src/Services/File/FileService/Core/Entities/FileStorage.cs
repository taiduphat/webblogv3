﻿using SharedCore;
using System.ComponentModel.DataAnnotations;

namespace FileService.Core.Entities
{
    public class FileStorage : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
