﻿using FileService.Core.Dtos;
using SharedCore;

namespace FileService.Core.Interfaces
{
    public interface IFileStorageService
    {
        Task<ServiceResult<Guid>> UploadFile(string fileName, byte[] content, bool shouldOverwrite);
        Task<ServiceResult<PagedList<FileStorageInfoDto>>> GetFileInfos(int page, int pageSize);
        Task<ServiceResult<PagedList<FileStorageDto>>> GetFiles(int page, int pageSize);
        Task<ServiceResult<bool>> RemoveFile(Guid fileId);
    }

}
