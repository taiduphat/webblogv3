﻿using FileService.Core.Entities;
using Microsoft.EntityFrameworkCore;
using SharedCore;

namespace FileService.Core.Interfaces
{
    public interface IFileDbContext : IApplicationDbContext
    {
        public DbSet<FileStorage> FileStorages { get; set; }
        public Task<int> SaveChangesAsync();
    }
}
