﻿using SharedCore;

namespace FileService.Core.Interfaces
{
    public interface IFileStorage
    {
        Task UploadFile(string filePath, byte[] content);
        Task<bool> RemoveFile(string filePath);
        Task<byte[]> GetFileContent(string filePath);
        string GetFilePath(string fileName);
    }

}
