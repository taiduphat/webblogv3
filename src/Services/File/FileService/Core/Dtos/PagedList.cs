﻿namespace FileService.Core.Dtos
{
    public class PagedList<T> where T : class
    {
        public IList<T> Results { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }

        public int PageCount => RowCount/PageSize + 1;

        public PagedList()
        {
            Results = new List<T>();
        }
    }
}
