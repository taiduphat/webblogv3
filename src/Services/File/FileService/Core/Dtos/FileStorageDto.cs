﻿namespace FileService.Core.Dtos
{
    public class FileStorageDto : FileStorageInfoDto
    {
        public byte[] Content { get; set; } = default!;
    }

    public class FileStorageInfoDto
    {
        public Guid? Id { get; set; }
        public string FileName { get; set; } = default!;
        public DateTime CreatedOn { get; set; }
    }
}
