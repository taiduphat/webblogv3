﻿using FileService.Core.Dtos;
using FileService.Core.Entities;
using FileService.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using SharedCore;

namespace FileService.Core.Services
{
    public class FileStorageService : IFileStorageService
    {
        private readonly IFileDbContext _fileDbContext;
        private readonly IFileStorage _fileStorage;

        public FileStorageService(IFileDbContext fileDbContext, IFileStorage fileStorage)
        {
            _fileDbContext = fileDbContext;
            _fileStorage = fileStorage;
        }

        public async Task<ServiceResult<PagedList<FileStorageInfoDto>>> GetFileInfos(int page = 0, int pageSize = 10)
        {
            var query = _fileDbContext.FileStorages.AsQueryable();

            int count = await query.CountAsync();

            var fileEntities = await query
                .Skip(page * pageSize)
                .Take(pageSize)
                .ToListAsync();
            var fileInfoDtos = fileEntities
                .Select(file => new FileStorageInfoDto
                {
                    Id = file.Id,
                    CreatedOn = file.CreatedOn,
                    FileName = file.Name
                }).ToList();

            var result = new PagedList<FileStorageInfoDto>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = fileEntities.Count,
                Results = fileInfoDtos
            };
            return ServiceResult<PagedList<FileStorageInfoDto>>.Success(result);
        }

        public async Task<ServiceResult<PagedList<FileStorageDto>>> GetFiles(int page, int pageSize)
        {
            var fileInfoDtosResponse = await GetFileInfos(page, pageSize);

            if (fileInfoDtosResponse == null || !fileInfoDtosResponse.IsSuccess)
            {
                return ServiceResult<PagedList<FileStorageDto>>.Success(new PagedList<FileStorageDto>());
            }

            var pagedFileInfosResult = fileInfoDtosResponse.Result;
            var fileDtos = new List<FileStorageDto>();
            foreach (var fileInfo in pagedFileInfosResult.Results)
            {
                var fileDto = (FileStorageDto)fileInfo;
                var filePath = _fileStorage.GetFilePath(fileInfo.FileName);
                fileDto.Content = await _fileStorage.GetFileContent(filePath);

                fileDtos.Add(fileDto);
            }

            var pagedListResult = new PagedList<FileStorageDto>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = pagedFileInfosResult.PageCount,
                Results = fileDtos
            };

            return ServiceResult<PagedList<FileStorageDto>>.Success(pagedListResult);
        }

        public async Task<ServiceResult<bool>> RemoveFile(Guid fileId)
        {
            var fileEntity = await _fileDbContext.FileStorages
                .FindAsync(fileId);

            if (fileEntity == null)
                return ServiceResult<bool>.Fail(StatusCode.FileNotFound);

            var filePath = _fileStorage.GetFilePath(fileEntity.Id.ToString());
            await _fileStorage.RemoveFile(filePath);

            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<Guid>> UploadFile(string fileName, byte[] content, bool shouldOverwrite)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return ServiceResult<Guid>.Fail(StatusCode.InvalidFileName);

            var fileEntity = await _fileDbContext.FileStorages
                .AsQueryable()
                .SingleOrDefaultAsync(x => x.Name.Equals(fileName));

            if (fileEntity != null && !shouldOverwrite)
                return ServiceResult<Guid>.Fail(StatusCode.FileNameAlreadyExisted);

            if (fileEntity == null)
            {
                fileEntity = new FileStorage
                {
                    Id = Guid.NewGuid(),
                    CreatedOn = DateTime.UtcNow,
                    Name = fileName
                };
                _fileDbContext.FileStorages.Add(fileEntity);
            }

            var filename = fileEntity.Id.ToString();
            var filePath = _fileStorage.GetFilePath(filename);
            await _fileStorage.UploadFile(filePath, content);

            await _fileDbContext.SaveChangesAsync();

            return ServiceResult<Guid>.Success(fileEntity.Id);

        }
    }
}
