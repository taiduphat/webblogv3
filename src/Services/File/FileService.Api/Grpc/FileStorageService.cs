﻿using FileService.Core.Interfaces;
using FileStorageApi;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;

namespace FileService.Api.Grpc
{
    public class FileStorageService : FileStorage.FileStorageBase
    {
        private readonly IFileStorageService _fileStorageService;

        public FileStorageService(IFileStorageService fileStorageService)
        {
            _fileStorageService = fileStorageService;
        }

        public override async Task<GetFilesResponse> GetFiles(GetFilesRequest request, ServerCallContext context)
        {
            var fileInfoDtosResult = await _fileStorageService.GetFiles(request.Page, request.PageSize);

            var getFilesResponse = new GetFilesResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = fileInfoDtosResult.IsSuccess,
                    StatusCode = fileInfoDtosResult.StatusCode.ToString()
                },
            };

            if (fileInfoDtosResult.IsSuccess)
            {
                getFilesResponse.PageCount = fileInfoDtosResult.Result.PageCount;

                var fileResponse = fileInfoDtosResult.Result.Results.Select(
                    file => new FileResponse
                    {
                        Id = file.Id.ToString(),
                        FileName = file.FileName,
                        Content = Google.Protobuf.ByteString.CopyFrom(file.Content),
                        CreatedOn = Timestamp.FromDateTime(file.CreatedOn)
                    });
            }

            return getFilesResponse;
        }

        public override async Task<BaseResponse> RemoveFile(RemoveFileRequest request, ServerCallContext context)
        {
            var fileId = Guid.Parse(request.FileId);
            var removeFileResponse = await _fileStorageService.RemoveFile(fileId);
            return new BaseResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = removeFileResponse.IsSuccess,
                    StatusCode = removeFileResponse.StatusCode.ToString()
                }
            };
        }

        public override async Task<BaseResponse> UploadFile(UploadFileRequest request, ServerCallContext context)
        {
            var uploadResponse = await _fileStorageService.UploadFile(request.FileName, request.Content.ToArray(), request.IsOverwrite);
            return new BaseResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = uploadResponse.IsSuccess,
                    StatusCode = uploadResponse.StatusCode.ToString()
                }
            };
        }
    }
}
