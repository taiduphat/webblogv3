using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;

namespace FileService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddLocalization();
            services.AddFileService(Configuration);

            services.AddHttpContextAccessor();

            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
            });

            this.ConfigureAuth(services);
        }

        protected virtual void ConfigureAuth(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var identityServerSection = Configuration.GetSection("IdentityServer");
                    string authority = identityServerSection.GetValue<string>("Authority");
                    string audience = identityServerSection.GetValue<string>("WebBlogApi:Audience");

                    options.Authority = authority;
                    options.Audience = audience;
                    options.RequireHttpsMetadata = false;
                });

            services.AddAuthorization();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                IdentityModelEventSource.ShowPII = true;
            }

            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);

            //app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCors(options => 
            { 
                options
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<Api.Grpc.FileStorageService>();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Template service up and runing.");
                });
            });
        }
    }
}
