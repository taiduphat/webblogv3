using Microsoft.EntityFrameworkCore;
using FileService.Infrastructure.Persistence;

namespace FileService.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHost(args);

            // Create database if not exists
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                var dbContext = services.GetRequiredService<FileDbContext>();
                dbContext.Database.Migrate();
            }

            host.Run();
        }

        public static IHost CreateWebHost(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder
                       .UseStartup<Startup>()
                       .UseKestrel(options =>
                       {
                           options.ListenAnyIP(80, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1AndHttp2);
                           options.ListenAnyIP(50071, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2);
                       })
                       .ConfigureAppConfiguration((hostContext, builder) =>
                       {
                           builder.AddJsonFile("appsettings.json", true, false);
                           builder.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true, false);

                           if (hostContext.HostingEnvironment.IsDevelopment())
                               builder.AddUserSecrets<Startup>();

                           builder.AddEnvironmentVariables();
                       });
               })
               .ConfigureLogging(loggingBuilder =>
               {
                   loggingBuilder.AddSystemdConsole(options =>
                   {
                       options.IncludeScopes = true;
                       options.TimestampFormat = "[MM/dd/yyyy HH:mm:ss.fff] ";
                   });
               });

            return hostBuilder.Build();
        }
    }
}
