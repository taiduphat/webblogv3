﻿using AutoMapper;
using ItemApi;
using ItemService.Core.Dtos;
using SharedApi.Extensions;
using SharedApi.Helpers;

namespace ItemService.Api
{
    public class GrpcMappings : Profile
    {
        public GrpcMappings()
        {
            CreateMap<ItemValueRequest, ItemValueDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)));
            CreateMap<InsertOrUpdateItemRequest, ItemDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)))
                .ForMember(dest => dest.ParentItemId, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.ParentItemId)))
                .ForMember(dest => dest.Values, opt => opt.MapFrom(x => x.ItemValues));


            CreateMap<ItemValueDto, ItemValueResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()));
                //.ForMember(dest => dest.FieldName, opt => opt.MapFrom(x => x.Field.Name));
            CreateMap<ItemDto, ItemResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.Id.HasValue ? x.Id.ToString() : string.Empty))
                .ForMember(dest => dest.ParentItemId, opt => opt.MapFrom(x => x.ParentItemId.HasValue ? x.ParentItemId.ToString() : string.Empty))
                .ForMember(dest => dest.ItemValues, opt => opt.MapFrom(x => x.Values))
                .ForMember(dest => dest.ChildItems, opt => opt.MapFrom(x => x.ChildItems));
        }
    }
}
