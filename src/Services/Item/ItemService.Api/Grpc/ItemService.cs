﻿using AutoMapper;
using Grpc.Core;
using ItemApi;
using ItemService.Api.Resources;
using ItemService.Core.Dtos;
using ItemService.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using SharedCore;

namespace ItemService.Api.Grpc
{
    [Authorize()]
    public class ItemService : Item.ItemBase
    {
        private readonly IItemService _itemService;
        private readonly IStringLocalizer<SharedResource> _stringLocalizer;
        private readonly IMapper _mapper;

        public ItemService(IItemService itemService, IStringLocalizer<SharedResource> stringLocalizer, IMapper mapper)
        {
            _itemService = itemService;
            _stringLocalizer = stringLocalizer;
            _mapper = mapper;
        }

        public override async Task<GetItemByIdResponse> GetItemById(GetItemByIdRequest request, ServerCallContext context)
        {
            var itemId = Guid.Parse(request.ItemId);
            var serviceResult = await _itemService.GetItemById(itemId);

            var model = new GetItemByIdResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = serviceResult.IsSuccess,
                    Message = _stringLocalizer[serviceResult.StatusCode.ToString()]
                }
            };

            if (serviceResult.Result != null)
                model.Item = _mapper.Map<ItemResponse>(serviceResult.Result);

            return model;
        }

        public override async Task<GetItemsResponse> GetItems(GetItemsRequest request, ServerCallContext context)
        {
            Guid? parentItemId = string.IsNullOrWhiteSpace(request.ParentItemId) ? null : Guid.Parse(request.ParentItemId);
            var serviceResult = await _itemService.GetItems(parentItemId, request.IsGetDescendants);

            var model = new GetItemsResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = serviceResult.IsSuccess,
                    Message = _stringLocalizer[serviceResult.StatusCode.ToString()]
                }
            };

            if (serviceResult.Result?.Count > 0)
            {
                var items = _mapper.Map<IList<ItemResponse>>(serviceResult.Result);
                model.Items.AddRange(items);
            }

            return model;

        }

        public override async Task<GetItemsByTemplateIdResponse> GetItemsByTemplateId(GetItemsByTemplateIdRequest request, ServerCallContext context)
        {
            var templateId = Guid.Parse(request.TemplateId);
            var serviceResult = await _itemService.GetItemsByTemplate(templateId);

            var model = new GetItemsByTemplateIdResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = serviceResult.IsSuccess,
                    Message = _stringLocalizer[serviceResult.StatusCode.ToString()]
                }
            };
            if (serviceResult.Result?.Count > 0)
            {
                var items = _mapper.Map<IList<ItemResponse>>(serviceResult.Result);
                model.Items.AddRange(items);
            }

            return model;
        }

        public override async Task<InsertOrUpdateItemResponse> InsertOrUpdateItem(InsertOrUpdateItemRequest request, ServerCallContext context)
        {
            var entityDto = _mapper.Map<ItemDto>(request);
            ServiceResult<Guid> serviceResult;

            if (entityDto.Id.HasValue)
                serviceResult = await _itemService.UpdateItem(entityDto);
            else
                serviceResult = await _itemService.InsertItem(entityDto);

            var model = new InsertOrUpdateItemResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = serviceResult.IsSuccess,
                    Message = _stringLocalizer[serviceResult.StatusCode.ToString()]
                }
            };

            if (serviceResult.IsSuccess)
                model.ItemId = serviceResult.Result.ToString();

            return model;
        }

        public override async Task<BaseResponse> RemoveItem(RemoveItemRequest request, ServerCallContext context)
        {
            Guid? itemId = string.IsNullOrWhiteSpace(request.ItemId) ? null : Guid.Parse(request.ItemId);
            if (!itemId.HasValue)
            {
                string message = string.Format(_stringLocalizer["InvalidGuidError"], nameof(request.ItemId));
                return new BaseResponse
                {
                    Status = new StatusResult
                    {
                        Message = message,
                    }
                };
            }

            var serviceResult = await _itemService.Remove(itemId.Value);

            var model = new BaseResponse
            {
                Status = new StatusResult
                {
                    IsSuccess = serviceResult.IsSuccess,
                    Message = _stringLocalizer[serviceResult.StatusCode.ToString()]
                }
            };
            return model;
        }
    }
}
