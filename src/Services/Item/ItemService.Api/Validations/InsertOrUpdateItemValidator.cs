﻿using FluentValidation;
using ItemApi;
using ItemService.Api.Resources;
using Microsoft.Extensions.Localization;
using SharedApi.Helpers;

namespace ItemService.Api.Validations
{
    public class InsertOrUpdateItemValidator : AbstractValidator<InsertOrUpdateItemRequest>
    {
        public InsertOrUpdateItemValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.Stop;

            When(m => !string.IsNullOrEmpty(m.Id), () =>
            {
                RuleFor(m => m.Id).Must(m => CommonHelper.IsEmptyOrValidGuid(m))
                    .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.Id)));
            });

            RuleFor(m => m.Name).Must(x => !string.IsNullOrEmpty(x))
               .WithMessage(x => string.Format(stringLocalizer["RequiredError"], nameof(x.Name)));

            When(m => !string.IsNullOrEmpty(m.Slug), () =>
            {
                RuleFor(m => m.Slug).Must(m => CommonHelper.IsMatchedRegex(m))
                    .WithMessage(x => string.Format(stringLocalizer["InvalidError"], nameof(x.Slug)));
            });

            RuleFor(m => m.TemplateId).Must(x => CommonHelper.IsValidGuid(x))
               .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.TemplateId)));

            When(m => CommonHelper.IsValidGuid(m.TemplateId), () =>
            {
                RuleFor(m => m.ItemValues)
                    .NotEmpty()
                    .WithMessage(x => string.Format(stringLocalizer["RequiredError"], nameof(x.ItemValues)));
            });

            When(m => m.ItemValues.Any(), () =>
            {
                RuleFor(x => x.ItemValues).ForEach(rules =>
                {
                    rules.Must(field => CommonHelper.IsEmptyOrValidGuid(field.Id))
                       .WithMessage((list, item) => string.Format(stringLocalizer["InvalidGuidError"], nameof(item.Id)));

                    rules.Must(field => CommonHelper.IsValidGuid(field.FieldId))
                        .WithMessage((list, item) => string.Format(stringLocalizer["InvalidGuidError"], nameof(item.FieldId)));

                    rules.Must(field => !string.IsNullOrWhiteSpace(field.Id) || !string.IsNullOrWhiteSpace(field.Value))
                        .WithMessage((list, item) => string.Format(stringLocalizer["RequiredError"], nameof(item.Value)));
                });

                RuleFor(x => x.ItemValues).Must(fields => !fields.GroupBy(x => x.FieldId).Any(g => g.Count() > 1))
                    .WithMessage(x => string.Format(stringLocalizer["DuplicatedError"], nameof(x.ItemValues)));
            });
        }
    }
}
