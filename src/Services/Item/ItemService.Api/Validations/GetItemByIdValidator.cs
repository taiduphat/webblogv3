﻿using FluentValidation;
using ItemApi;
using ItemService.Api.Resources;
using Microsoft.Extensions.Localization;
using SharedApi.Helpers;

namespace ItemService.Api.Validations
{
    public class GetItemByIdValidator : AbstractValidator<GetItemByIdRequest>
    {
        public GetItemByIdValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m.ItemId).Must(x => CommonHelper.IsValidGuid(x))
                .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.ItemId)));
        }
    }
}
