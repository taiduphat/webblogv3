﻿using AutoMapper;
using ItemService.Core.Dtos;
using ItemService.Core.Entities;

namespace ItemService
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ItemValueDto, ItemValue>();
            CreateMap<ItemValue, ItemValueDto>();

            CreateMap<ItemDto, Item>()
                .ForMember(pro => pro.Values, opt => opt.Ignore());
            CreateMap<Item, ItemDto>();
        }
    }
}
