﻿using ItemService.Core.Entities;
using ItemService.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SharedInfrastructure;

namespace ItemService.Infrastructure.Persistence
{
    /// <summary>
    /// Implement of the IDesignTimeDbContextFactory 
    /// Supporting for generating the EF migration at design-time (EF code-first approach). Meaning that we could creating migration in this project as well.
    /// </summary>
    public class ItemDbContextDesignFactory : DbContextDesignFactoryBase<ItemDbContext>
    {
    }

    public class ItemDbContext : DbContext, IItemDbContext
    {
        public DbSet<Item> Items { get; set; } = default!;
        public DbSet<ItemValue> ItemValues { get; set; } = default!;

        public ItemDbContext(DbContextOptions options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.InitialDateTimeConverter();

            var entityTypes = modelBuilder.Model.GetEntityTypes();
            // Seeds data
            foreach (var relationship in entityTypes.SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }
    }
}
