﻿using ItemService.Core.Dtos;
using ItemService.Core.Gateway;

namespace ItemService.Infrastructure.Gateway
{
    internal class TemplateGateway : ITemplateGateway
    {
        private readonly TemplateApi.Template.TemplateClient _templateClient;

        public TemplateGateway(TemplateApi.Template.TemplateClient templateClient)
        {
            _templateClient = templateClient;
        }


        public async Task<IEnumerable<FieldDto>> GetFieldsByTemplateId(Guid templateId)
        {
            var getFieldsRequest = new TemplateApi.GetTemplateByIdRequest 
            { 
                TemplateId = templateId.ToString() 
            };
            var templateResponse = await _templateClient.GetTemplateByIdAsync(getFieldsRequest);
            var fieldsResponse = templateResponse.Template.Fields;

            return fieldsResponse
                .Select(x => new FieldDto
                {
                    Id = Guid.Parse(x.Id),
                    FieldTypeId = Guid.Parse(x.FieldTypeId),
                    DataSource = x.DataSource
                }).ToList();
        }
    }
}
