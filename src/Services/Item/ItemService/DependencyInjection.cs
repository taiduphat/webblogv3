﻿using ItemService.Core.Gateway;
using ItemService.Core.Interfaces;
using ItemService.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedCore;
using SharedCore.Config;

namespace ItemService
{
    public static class DependencyInjection
    {
        public static void AddItemService(this IServiceCollection services, IConfiguration configuration)
        {
            // Core services
            services.AddScoped<IItemService, Core.Services.ItemService>();
            services.AddScoped<ITemplateGateway, Infrastructure.Gateway.TemplateGateway>();

            // DbContext 
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<ItemDbContext>(options => options.UseInMemoryDatabase("Item"));
            }
            else
            {
                services.AddDbContext<ItemDbContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                        b => b.MigrationsAssembly(typeof(ItemDbContext).Assembly.FullName)));
            }
            services.AddScoped<IApplicationDbContext>(provider => provider.GetRequiredService<ItemDbContext>());

            // Grpc
            services.AddGrpcClient<TemplateApi.Template.TemplateClient>(o =>
            {
                var itemApiConfig = configuration.GetValue<ApiConfig>("TemplateApi");
                
                UriBuilder uriBuilder = new();
                uriBuilder.Scheme = itemApiConfig.Scheme;
                uriBuilder.Host = itemApiConfig.Host;
                uriBuilder.Port = itemApiConfig.Port;

                o.Address = uriBuilder.Uri;
            });
        }
    }
}
