﻿namespace ItemService.Core.Dtos
{
    public class FieldDto
    {
        public Guid Id { get; set; }
        public Guid FieldTypeId { get; set; }
        public string DataSource { get; set; }
    }
}
