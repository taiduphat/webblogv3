﻿namespace ItemService.Core.Dtos
{
    public class ItemValueDto
    {
        public Guid? Id { get; set; }
        public Guid ItemId { get; set; }
        public string Value { get; set; } = default!;
        public Guid FieldId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
