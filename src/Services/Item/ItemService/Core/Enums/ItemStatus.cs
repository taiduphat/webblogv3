﻿namespace ItemService.Core.Enums
{
    public enum ItemStatus
    {
        Draft,
        Published,
        Trash
    }
}
