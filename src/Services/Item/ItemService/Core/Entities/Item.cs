﻿using ItemService.Core.Enums;
using SharedCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ItemService.Core.Entities
{
    public class Item : BaseEntity
    {
        public Guid TemplateId { get; set; }
        [ForeignKey(nameof(Item.TemplateId))]

        public Guid? ParentItemId { get; set; }
        [ForeignKey(nameof(Item.ParentItemId))]
        public virtual Item ParentItem { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public ItemStatus ItemStatus { get; set; } 

        [Required]
        public string Name { get; set; }
        [Required]
        public string Slug { get; set; }
        public DateTime? PublishedOn { get; set; }

        public virtual ICollection<ItemValue> Values { get; set; }

        public Item()
        {
            this.Values = new HashSet<ItemValue>();
        }
    }
}
