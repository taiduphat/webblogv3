﻿using AutoMapper;
using ItemService.Core.Dtos;
using ItemService.Core.Entities;
using ItemService.Core.Enums;
using ItemService.Core.Gateway;
using ItemService.Core.Interfaces;
using Microsoft.EntityFrameworkCore;
using SharedCore;

namespace ItemService.Core.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemDbContext _dbContext;
        private readonly ITemplateGateway _templateGateway;
        private readonly IMapper _mapper;

        public ItemService(IApplicationDbContext dbContext,
            ITemplateGateway templateGateway,
            IMapper mapper)
        {
            _dbContext = (IItemDbContext)dbContext;
            _templateGateway = templateGateway;
            _mapper = mapper;
        }

        public async Task<ServiceResult<ItemDto>> GetItemById(Guid id)
        {
            var item = await _dbContext.Items
                .Include(x => x.Values)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (item == null)
                return ServiceResult<ItemDto>.Fail(StatusCode.ItemNotFound);

            return ServiceResult<ItemDto>.Success(_mapper.Map<ItemDto>(item));
        }

        public async Task<ServiceResult<List<ItemDto>>> GetItems(Guid? parentItemId, bool isGetDescendants)
        {
            if (isGetDescendants)
            {
                var itemDtos = await GetItemHierarchy(parentItemId);
                return ServiceResult<List<ItemDto>>.Success(itemDtos);
            }
            else
            {
                var matchedItems = await _dbContext.Items
                    .Where(x => x.ParentItemId == parentItemId)
                    .ToListAsync();
                var itemIds = matchedItems.Select(x => (Guid?)x.Id).ToList();

                var children = await _dbContext.Items
                    .Where(x => itemIds.Contains(x.ParentItemId))
                    .ToListAsync();
                var groupResult = matchedItems.GroupJoin(children,
                    left => left.Id,
                    right => right.ParentItemId,
                    (left, right) => new { left, HasChildren = right.Any() });

                var result = new List<ItemDto>();
                foreach (var group in groupResult)
                {
                    var temp = _mapper.Map<ItemDto>(group.left);
                    temp.HasChildren = group.HasChildren;

                    result.Add(temp);
                }

                return ServiceResult<List<ItemDto>>.Success(result);
            }
        }

        public async Task<ServiceResult<Guid>> InsertItem(ItemDto itemDto)
        {
            var fields = await _templateGateway.GetFieldsByTemplateId(itemDto.TemplateId);
            var exceptedFields = itemDto.Values
                .Select(x => x.FieldId)
                .Except(fields.Select(x => x.Id))
                .Any();
            if (exceptedFields)
                return ServiceResult<Guid>.Fail(StatusCode.FieldNotFound);

            if (itemDto.ParentItemId.HasValue)
            {
                bool isValidParentItem = await _dbContext.Items.AnyAsync(x => x.Id == itemDto.ParentItemId);
                if (!isValidParentItem)
                    return ServiceResult<Guid>.Fail(StatusCode.ParentItemNotFound);
            }

            var entity = _mapper.Map<Item>(itemDto);
            entity.Id = Guid.NewGuid();
            entity.CreatedOn = DateTime.UtcNow;
            entity.ItemStatus = ItemStatus.Draft;

            foreach (var item in itemDto.Values)
            {
                item.Id ??= Guid.NewGuid();
                var itemValue = _mapper.Map<ItemValue>(item);
                itemValue.CreatedOn = DateTime.UtcNow;

                entity.Values.Add(itemValue);
            }

            _dbContext.Items.Add(entity);
            await _dbContext.SaveChangesAsync();

            return ServiceResult<Guid>.Success(entity.Id);
        }

        public async Task<ServiceResult<Guid>> UpdateItem(ItemDto itemDto)
        {
            try
            {
                var validateResult = await IsItemValidForUpdate(itemDto);
                if (!validateResult.IsSuccess)
                    return ServiceResult<Guid>.Fail(validateResult.StatusCode);


                var beforeEdit = await _dbContext.Items
                    .Include(x => x.Values)
                    .FirstAsync(x => x.Id == itemDto.Id.Value);

                beforeEdit.UpdatedOn = DateTime.UtcNow;
                beforeEdit.Name = itemDto.Name;
                beforeEdit.Slug = itemDto.Slug;
                beforeEdit.ParentItemId = itemDto.ParentItemId;

                // update existed item values
                var joinResult = from entityValue in beforeEdit.Values
                                 join requestValue in itemDto.Values on entityValue.Id equals requestValue.Id
                                 select (entityValue, requestValue);
                foreach (var (entityValue, requestValue) in joinResult)
                {
                    entityValue.FieldId = requestValue.FieldId;
                    entityValue.Value = requestValue.Value;
                    entityValue.UpdatedOn = DateTime.UtcNow;
                }

                _dbContext.Items.Update(beforeEdit);

                // add new item values
                var newValueDtos = itemDto.Values
                    .Where(x => !x.Id.HasValue)
                    .Select(item => new ItemValue
                    {
                        ItemId = beforeEdit.Id,
                        Id = Guid.NewGuid(),
                        FieldId = item.FieldId,
                        Value = item.Value,
                        CreatedOn = DateTime.UtcNow,
                    });
                _dbContext.ItemValues.AddRange(newValueDtos);

                await _dbContext.SaveChangesAsync();

                return ServiceResult<Guid>.Success(beforeEdit.Id);
            }
            catch (Exception)
            {
                return ServiceResult<Guid>.Fail(StatusCode.InternalServerError);
            }
        }

        protected async Task<ServiceResult<bool>> IsItemValidForUpdate(ItemDto itemDto)
        {
            var beforeEdit = await _dbContext.Items
                    .Include(x => x.Values)
                    .FirstAsync(x => x.Id == itemDto.Id.Value);

            if (beforeEdit == null)
                return ServiceResult<bool>.Fail(StatusCode.ItemNotFound);
            else if (beforeEdit.TemplateId != itemDto.TemplateId)
                return ServiceResult<bool>.Fail(StatusCode.TemplateNotMatching);

            // updated fields should be template's fields
            var fields = await _templateGateway.GetFieldsByTemplateId(itemDto.TemplateId);
            var exceptIds = itemDto.Values
                .Select(x => x.FieldId)
                .Except(fields.Select(x => x.Id));
            if (exceptIds.Any())
                return ServiceResult<bool>.Fail(StatusCode.FieldNotFound);

            // updated item value should be existed
            var isExistedValue = !itemDto.Values.Any(x => x.Id.HasValue && !beforeEdit.Values.Any(e => e.Id == x.Id.Value));
            if (!isExistedValue)
                return ServiceResult<bool>.Fail(StatusCode.ItemValueNotFound);

            // The value of fields of this item should have only one value
            var filterFieldIds = beforeEdit.Values
                .Select(x => x.FieldId).ToList();
            isExistedValue = itemDto.Values.Any(x => !x.Id.HasValue && filterFieldIds.Any(fieldId => fieldId == x.FieldId));
            if (isExistedValue)
                return ServiceResult<bool>.Fail(StatusCode.ItemValueAlreadyExisted);

            if (itemDto.ParentItemId.HasValue)
            {
                bool isValidParentItem = await _dbContext.Items.AnyAsync(x => x.Id == itemDto.ParentItemId);
                if (!isValidParentItem)
                    return ServiceResult<bool>.Fail(StatusCode.ParentItemNotFound);
            }

            return ServiceResult<bool>.Success(true);
        }

        public async Task<List<ItemDto>> GetItemHierarchy(Guid? parentItemId)
        {
            var matchedItems = await _dbContext.Items
                .Where(x => x.ParentItemId == parentItemId)
                .ToListAsync();

            var itemDtos = _mapper.Map<List<ItemDto>>(matchedItems);
            foreach (var item in itemDtos)
            {
                item.ChildItems = await GetItemHierarchy(item.Id);
                item.HasChildren = item.ChildItems.Count != 0;
            }

            return itemDtos;
        }

        protected IEnumerable<Guid> GetAllItemIds(IList<ItemDto> items)
        {
            var result = items.Select(x => x.Id.Value).ToList();
            foreach (var item in items)
            {
                result.AddRange(GetAllItemIds(item.ChildItems));
            }

            return result;
        }

        public async Task<ServiceResult<bool>> Remove(Guid id)
        {
            bool isExisted = await _dbContext.Items.AnyAsync(x => x.Id == id);
            if (!isExisted)
            {
                return ServiceResult<bool>.Fail(StatusCode.ItemNotFound);
            }

            var itemHirerachy = await GetItemHierarchy(id);
            var removeItemIds = GetAllItemIds(itemHirerachy).ToList();
            removeItemIds.Add(id);

            var entities = await _dbContext.Items
                .Where(x => removeItemIds.Contains(x.Id))
                .ToListAsync();
            
            _dbContext.Items.RemoveRange(entities);
            await _dbContext.SaveChangesAsync();

            return ServiceResult<bool>.Success(true);
        }

        public async Task<ServiceResult<List<ItemDto>>> GetItemsByTemplate(Guid templateId)
        {
            var items = await _dbContext.Items
                .Include(x => x.Values)
                .Where(x => x.TemplateId == templateId)
                .ToListAsync();

            var result = _mapper.Map<List<ItemDto>>(items);
            return ServiceResult<List<ItemDto>>.Success(result);
        }
    }

}
