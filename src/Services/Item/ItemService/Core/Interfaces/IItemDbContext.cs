﻿using ItemService.Core.Entities;
using Microsoft.EntityFrameworkCore;
using SharedCore;

namespace ItemService.Core.Interfaces
{
    public interface IItemDbContext : IApplicationDbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemValue> ItemValues { get; set; }
        public Task<int> SaveChangesAsync();
    }
}
