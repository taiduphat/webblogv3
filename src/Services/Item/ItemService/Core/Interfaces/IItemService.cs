﻿using ItemService.Core.Dtos;
using SharedCore;

namespace ItemService.Core.Interfaces
{
    public interface IItemService
    {
        Task<ServiceResult<Guid>> InsertItem(ItemDto itemDto);
        Task<ServiceResult<List<ItemDto>>> GetItems(Guid? parentItemId, bool isGetDescendants);
        Task<ServiceResult<List<ItemDto>>> GetItemsByTemplate(Guid templateId);
        Task<ServiceResult<ItemDto>> GetItemById(Guid id);
        Task<ServiceResult<Guid>> UpdateItem(ItemDto itemDto);
        Task<ServiceResult<bool>> Remove(Guid id);
    }
}
