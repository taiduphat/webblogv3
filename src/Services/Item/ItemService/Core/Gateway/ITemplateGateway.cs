﻿using ItemService.Core.Dtos;

namespace ItemService.Core.Gateway
{
    public interface ITemplateGateway
    {
        Task<IEnumerable<FieldDto>> GetFieldsByTemplateId(Guid templateId);
    }
}
