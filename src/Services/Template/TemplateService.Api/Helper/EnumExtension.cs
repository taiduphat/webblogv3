﻿using Microsoft.Extensions.Localization;
using TemplateService.Api.Resources;

namespace TemplateService.Api.Helper
{
    public static class EnumExtension
    {
        public static string GetMessage(this SharedCore.StatusCode statusCode, IStringLocalizer<SharedResource> stringLocalizer)
        {
            return stringLocalizer[statusCode.ToString()];
        }
    }
}
