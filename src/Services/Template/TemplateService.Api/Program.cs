using Microsoft.EntityFrameworkCore;
using TemplateService.Infrastructure.Persistence;

namespace TemplateService.Api
{
    public class Program
    {
        public static void Main(string[] args) 
            => CreateWebHost(args).Run();

        public static IHost CreateWebHost(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder
                       .UseStartup<Startup>()
                       .UseKestrel(options =>
                       {
                           options.ListenAnyIP(80, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1AndHttp2);
                           options.ListenAnyIP(50051, x => x.Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http2);
                       })
                       .ConfigureAppConfiguration((hostContext, builder) =>
                       {
                           builder.AddJsonFile("appsettings.json", true, false);
                           builder.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", true, false);

                           if (hostContext.HostingEnvironment.IsDevelopment())
                               builder.AddUserSecrets<Startup>();

                           builder.AddEnvironmentVariables();
                       });
               })
               .ConfigureLogging(loggingBuilder =>
               {
                   loggingBuilder.AddSystemdConsole(options =>
                   {
                       options.IncludeScopes = true;
                       options.TimestampFormat = "[MM/dd/yyyy HH:mm:ss.fff] ";
                   });
               });

            return hostBuilder.Build();
        }
    }
}
