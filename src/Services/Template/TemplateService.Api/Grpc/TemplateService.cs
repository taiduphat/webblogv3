﻿using AutoMapper;
using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using SharedApi.Helpers;
using SharedCore;
using TemplateApi;
using TemplateService.Api.Helper;
using TemplateService.Api.Resources;
using TemplateService.Core.Dtos;
using TemplateService.Core.Interfaces;

namespace TemplateService.Api.Grpc
{
    [Authorize()]
    public class TemplateService : Template.TemplateBase
    {
        private readonly ITemplateService _templateService;
        private readonly IStringLocalizer<SharedResource> _stringLocalizer;
        private readonly IMapper _mapper;
        public TemplateService(IMapper mapper,
            IStringLocalizer<SharedResource> stringLocalizer,
            ITemplateService templateService)
        {
            _templateService = templateService;
            _stringLocalizer = stringLocalizer;
            _mapper = mapper;
        }

        public override async Task<GetFieldTypesResponse> GetFieldTypes(Google.Protobuf.WellKnownTypes.Empty request, ServerCallContext context)
        {
            var serviceResult = await _templateService.GetFieldTypes();

            var model = GetResponse<GetFieldTypesResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result?.Count > 0)
            {
                var fieldTypes = _mapper.Map<IList<FieldType>>(serviceResult.Result);
                model.FieldTypes.AddRange(fieldTypes);
            }

            return model;
        }

        public override async Task<GetTemplateByIdResponse> GetTemplateById(GetTemplateByIdRequest request, ServerCallContext context)
        {
            var templateId = Guid.Parse(request.TemplateId);
            var serviceResult = await _templateService.GetTemplateById(templateId);

            var model = GetResponse<GetTemplateByIdResponse>(serviceResult.StatusCode, _stringLocalizer);
            if (serviceResult.Result != null)
                model.Template = _mapper.Map<TemplateResponse>(serviceResult.Result);

            return model;
        }

        public override async Task<GetTemplatesResponse> GetTemplates(GetTemplatesRequest request, ServerCallContext context)
        {
            var serviceResult = await _templateService.GetTemplates();
            var result = _mapper.Map<List<TemplateResponse>>(serviceResult.Result);

            var model = GetResponse<GetTemplatesResponse>(serviceResult.StatusCode, _stringLocalizer);
            model.Templates.AddRange(result);

            return model;
        }

       
        public override async Task<InsertOrUpdateTemplateResponse> InsertOrUpdateTemplate(InsertOrUpdateTemplateRequest request, ServerCallContext context)
        {
            var templateDto = _mapper.Map<TemplateDto>(request);
            ServiceResult<Guid> serviceResult = null;

            if (!templateDto.Id.HasValue)
                serviceResult = await _templateService.Insert(templateDto);
            else
                serviceResult = await _templateService.Update(templateDto);

            var model = GetResponse<InsertOrUpdateTemplateResponse>(serviceResult.StatusCode, _stringLocalizer);

            if (serviceResult.IsSuccess)
                model.TemplateId = serviceResult.Result.ToGuidString();

            return model;
        }

        public override async Task<BaseResponse> RemoveTemplate(RemoveTemplateRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.TemplateId, out var templateId))
            {
                string message = string.Format(_stringLocalizer["InvalidGuidError"], nameof(request.TemplateId));
                return CommonHelper.GetFailResponse<BaseResponse>(message);
            }

            var serviceResult = await _templateService.Remove(templateId);

            var model = GetResponse<BaseResponse>(serviceResult.StatusCode, _stringLocalizer);
            return model;
        }

        private TResponse GetResponse<TResponse>(SharedCore.StatusCode statusCode, IStringLocalizer<SharedResource> stringLocalizer)
        {
            string message = statusCode.GetMessage(stringLocalizer);
            return CommonHelper.GetResponse<TResponse>(statusCode == SharedCore.StatusCode.Success, message);
        }
    }
}
