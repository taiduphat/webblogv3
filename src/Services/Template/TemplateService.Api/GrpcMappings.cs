﻿using AutoMapper;
using SharedApi.Extensions;
using SharedApi.Helpers;
using TemplateApi;
using TemplateService.Core.Dtos;

namespace TemplateService.Api
{
    public class GrpcMappings : Profile
    {
        public GrpcMappings()
        {
            CreateMap<FieldTypeDto, FieldType>();

            CreateMap<FieldRequest, FieldDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)))
                .ForMember(dest => dest.DataSource, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.DataSource)));
            CreateMap<InsertOrUpdateTemplateRequest, TemplateDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(x => CommonHelper.ParseGuid(x.Id)));
            
            CreateMap<TemplateDto, TemplateResponse>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()))
                .ForMember(dest => dest.Fields, opt => opt.MapFrom(x => x.Fields));

            CreateMap<FieldDto, FieldResponse>()
                .ForMember(dest => dest.DataSource, opt => opt.MapFrom(x => x.DataSource.ToString() ?? string.Empty))
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(x => x.UpdatedOn.GetTimestamp()))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(x => ((DateTime?)x.CreatedOn).GetTimestamp()));
        }
    }
}
