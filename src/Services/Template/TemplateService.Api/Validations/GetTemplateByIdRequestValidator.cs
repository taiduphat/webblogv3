﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using SharedApi.Helpers;
using TemplateApi;
using TemplateService.Api.Resources;

namespace TemplateService.Api.Validations
{
    public class GetTemplateByIdRequestValidator : AbstractValidator<GetTemplateByIdRequest>
    {
        public GetTemplateByIdRequestValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(m => m.TemplateId).Must(x => CommonHelper.IsValidGuid(x))
                .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.TemplateId)));
        }
    }
}
