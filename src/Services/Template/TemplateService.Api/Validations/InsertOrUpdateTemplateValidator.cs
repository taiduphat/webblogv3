﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using SharedApi.Helpers;
using TemplateApi;
using TemplateService.Api.Resources;

namespace TemplateService.Api.Validations
{
    public class InsertOrUpdateTemplateValidator : AbstractValidator<InsertOrUpdateTemplateRequest>
    {
        public InsertOrUpdateTemplateValidator(IStringLocalizer<SharedResource> stringLocalizer)
        {
            CascadeMode = CascadeMode.Stop;

            When(m => !string.IsNullOrEmpty(m.Id), () =>
            {
                RuleFor(m => m.Id).Must(x => CommonHelper.IsEmptyOrValidGuid(x))
                 .WithMessage(x => string.Format(stringLocalizer["InvalidGuidError"], nameof(x.Id)));
            });

            RuleFor(m => m.Name).Must(x => !string.IsNullOrEmpty(x))
                .WithMessage(x => string.Format(stringLocalizer["RequiredError"], nameof(x.Name)));

            When(m => !string.IsNullOrEmpty(m.Slug), () =>
            {
                RuleFor(m => m.Slug).Must(m => CommonHelper.IsMatchedRegex(m))
                    .WithMessage(x => string.Format(stringLocalizer["InvalidError"], nameof(x.Slug)));
            });

            RuleFor(m => m.Fields)
                .NotEmpty()
                .WithMessage(x => string.Format(stringLocalizer["RequiredError"], nameof(x.Fields)));

            When(m => m.Fields.Any(), () =>
            {
                RuleFor(x => x.Fields).ForEach(rules => 
                {
                    rules.Must(field => CommonHelper.IsEmptyOrValidGuid(field.Id))
                        .WithMessage((list, item) => string.Format(stringLocalizer["InvalidGuidError"], nameof(item.Id)));

                    rules.Must(field => CommonHelper.IsValidGuid(field.FieldTypeId))
                        .WithMessage((list, item) => string.Format(stringLocalizer["InvalidGuidError"], nameof(item.FieldTypeId)));

                    rules.Must(field => CommonHelper.IsEmptyOrValidGuid(field.DataSource))
                        .WithMessage((list, item) => string.Format(stringLocalizer["InvalidGuidError"], nameof(item.DataSource)));

                    rules.Must(field => !string.IsNullOrEmpty(field.Name))
                        .WithMessage((list, item) => string.Format(stringLocalizer["RequiredError"], nameof(item.Name)));
                });

                RuleFor(x => x.Fields).Must(fields => !fields.GroupBy(x => x.Name).Any(g => g.Count() > 1))
                    .WithMessage(x => string.Format(stringLocalizer["DuplicatedError"], nameof(x.Fields)));
            });
        }
    }
}
