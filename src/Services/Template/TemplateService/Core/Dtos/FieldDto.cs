﻿namespace TemplateService.Core.Dtos
{
    public class FieldDto
    {
        public Guid? Id { get; set; }
        public Guid TemplateId { get; set; }
        public Guid FieldTypeId { get; set; }
        public Guid? DataSource { get; set; }
        public string Name { get; set; } = default!;
        public bool IsRequired { get; set; }
        public bool IsEditable { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
