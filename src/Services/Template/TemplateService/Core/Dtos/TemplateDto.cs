﻿using TemplateService.Core.Dtos;

namespace TemplateService.Core.Dtos
{
    public class TemplateDto
    {
        public Guid? Id { get; set; }
        public string Slug { get; set; } = default!;
        public string Name { get; set; } = default!;
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public IList<FieldDto> Fields { get; set; } = default!;
    }
}
