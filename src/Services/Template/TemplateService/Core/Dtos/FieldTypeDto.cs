﻿using TemplateService.Core.Enums;

namespace TemplateService.Core.Dtos
{
    public class FieldTypeDto
    {
        public Guid Id { get; set; }
        public FieldType Type { get; set; }
        public string AltName { get; set; } = default!;
        public string Description { get; set; } = default!;
    }
}
