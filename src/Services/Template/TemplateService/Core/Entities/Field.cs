﻿using SharedCore;
using System.ComponentModel.DataAnnotations;

namespace TemplateService.Core.Entities
{
    public class Field : BaseEntity
    {
        public Guid TemplateId { get; set; }
        public Guid FieldTypeId { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsRequired { get; set; }
        public bool IsEditable { get; set; }

        public Guid? DataSource { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
