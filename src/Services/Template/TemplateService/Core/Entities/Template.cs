﻿using SharedCore;
using System.ComponentModel.DataAnnotations;

namespace TemplateService.Core.Entities
{
    public class Template : BaseEntity
    {
        [Required]
        public string Slug { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public Template()
        {
        }
    }
}
