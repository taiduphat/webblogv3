﻿using AutoMapper;
using TemplateService.Core.Dtos;
using TemplateService.Core.Entities;

namespace TemplateService.Core
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<FieldTypeDto, FieldType>();
            CreateMap<FieldType, FieldTypeDto>();

            CreateMap<FieldDto, Field>();
            CreateMap<Field, FieldDto>();

            CreateMap<TemplateDto, Template>();
            CreateMap<Template, TemplateDto>();
        }
    }
}
