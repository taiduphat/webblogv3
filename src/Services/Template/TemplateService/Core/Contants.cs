﻿namespace TemplateService.Core
{
    public class Constants
    {
        protected Constants() { }

        public const string BoolId = "1b18f811-ebab-49c4-98f7-20df944ba876";
        public const string DateTimeId = "8c630622-45a0-4eda-8291-c70b47d290fb";
        public const string ItemRefId = "ccbeb21c-bd3a-4c75-92c0-d8f83467cb67";
        public const string ItemSetRefId = "473f7834-f806-4049-bb20-41aecf29c388";
        public const string LinkId = "dcf9c91b-d34d-4dce-8b74-10eab1e2be34";
        public const string NumberId = "94d86abc-5a4b-46ac-be43-ddeebd153290";
        public const string PlainTextId = "6201a71d-b4b6-43f9-9076-9a3218732422";
        public const string RichTextId = "37ba6712-3043-4392-a447-0d9cf8fa59d8";
        public const string FileRefId = "451a2d88-1ff6-47d8-91aa-29aa210eac6a";

        public const string PermissionClaimType = "permission";
    }
}
