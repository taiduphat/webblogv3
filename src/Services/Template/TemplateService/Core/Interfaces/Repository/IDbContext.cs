﻿using TemplateService.Core.Entities;

namespace TemplateService.Core.Interfaces.Repository
{
    public interface IDbContext
    {
        public ITemplateRepository Templates { get; }
        public IFieldRepository Fields { get; }
        public IGenericRepository<FieldType> FieldTypes { get; }
        public Task BeginTransactionAsync();
        public Task CommitAsync();
        public Task RollbackAsync();
    }
}
