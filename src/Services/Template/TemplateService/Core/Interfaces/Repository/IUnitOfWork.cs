﻿using System.Data;
using TemplateService.Core.Entities;

namespace TemplateService.Core.Interfaces.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IDbConnection Connection { get; }
        IDbTransaction? Transaction { get; }
        Task BeginTransactionAsync();
        Task CommitAsync();
        Task RollbackAsync();
    }
}
