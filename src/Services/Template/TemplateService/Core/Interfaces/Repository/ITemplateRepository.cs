﻿using TemplateService.Core.Entities;

namespace TemplateService.Core.Interfaces.Repository
{
    public interface ITemplateRepository : IGenericRepository<Template>
    {
        public Task<int> UpdateAsync(Template template);
    }
}
