﻿using SharedCore;

namespace TemplateService.Core.Interfaces.Repository
{
    public interface IGenericRepository<TEntity> 
        where TEntity : BaseEntity
    {
        Task<TEntity> GetByIdAsync(Guid id);
        Task<IReadOnlyList<TEntity>> GetAllAsync();
        Task<int> AddAsync(TEntity entity);
        Task<int> DeleteAsync(Guid id);
    }
}
