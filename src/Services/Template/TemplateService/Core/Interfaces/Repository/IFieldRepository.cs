﻿using SharedCore;
using TemplateService.Core.Entities;

namespace TemplateService.Core.Interfaces.Repository
{
    public interface IFieldRepository : IGenericRepository<Field>
    {
        public Task<List<Field>> GetFieldsByTemplateIdAsync(Guid templateId);
        public Task<int> UpdateAsync(Field field);
        public Task<int> DeletesAsync(IEnumerable<Guid> Ids);
    }
}
