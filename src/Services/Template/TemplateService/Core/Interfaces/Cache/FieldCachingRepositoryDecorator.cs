﻿using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Core.Interfaces.Cache
{
    public class FieldCachingRepositoryDecorator : RepositoryCachingDecorator<Field>, IFieldRepository
    {
        protected new IFieldRepository InnerRepository => (IFieldRepository)base.InnerRepository;
        public FieldCachingRepositoryDecorator(IFieldRepository innerRepository, ICacheService cacheService) 
            : base(innerRepository, cacheService)
        {
        }

        public async Task<int> DeletesAsync(IEnumerable<Guid> Ids)
        {
            var result = await InnerRepository.DeletesAsync(Ids);
            lock (LockObject)
            {
                foreach (var id in Ids)
                {
                    var key = GetCacheObjectKey(id);
                    CacheService.Remove(key);
                }
                
                // Clear cached object collection
                CacheService.Remove(GetCacheCollectionKey());
            }

            return result;
        }

        public async Task<List<Field>> GetFieldsByTemplateIdAsync(Guid templateId)
        {
            var key = string.Format("{0}_By_TemplateId_{1}", GetCacheCollectionKey(), templateId);
            if (CacheService.TryGet<List<Field>>(key, out var entities))
            {
                var searchKey = $"{EntityName}_Id";
                var relatedKeys = CacheService.FilterKeys(searchKey);
                var exceptItems = entities.Select(x => GetCacheObjectKey(x.Id))
                    .Except(relatedKeys);

                if (!exceptItems.Any())
                    return entities;
            }

            entities = await InnerRepository.GetFieldsByTemplateIdAsync(templateId);
            lock (LockObject)
            {
                // cached object collection
                CacheService.Set(key, entities);
                // cached individual item of collection
                foreach (var item in entities)
                {
                    var itemKey = GetCacheObjectKey(item.Id);
                    CacheService.Set(itemKey, item);
                }
            }

            return entities;
        }

        public async Task<int> UpdateAsync(Field field)
        {
            var result = await InnerRepository.UpdateAsync(field);
            lock (LockObject)
            {
                var key = GetCacheObjectKey(field.Id);
                CacheService.Set(key, field);

                // Clear cached object collection
                CacheService.Remove(GetCacheCollectionKey());
            }

            return result;
        }
    }
}
