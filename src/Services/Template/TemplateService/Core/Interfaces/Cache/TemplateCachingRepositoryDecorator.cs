﻿using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Core.Interfaces.Cache
{
    public class TemplateCachingRepositoryDecorator : RepositoryCachingDecorator<Template>, ITemplateRepository
    {
        protected new ITemplateRepository InnerRepository => (ITemplateRepository)base.InnerRepository;
        public TemplateCachingRepositoryDecorator(ITemplateRepository templateRepository,
            ICacheService cacheService) : base(templateRepository, cacheService)
        {
        }

        public async Task<int> UpdateAsync(Template template)
        {
            var result = await InnerRepository.UpdateAsync(template);
            lock (LockObject)
            {
                var key = GetCacheObjectKey(template.Id);
                CacheService.Set(key, template);

                // Clear cached object collection
                CacheService.Remove(GetCacheCollectionKey());
            }

            return result;
        }
    }
}
