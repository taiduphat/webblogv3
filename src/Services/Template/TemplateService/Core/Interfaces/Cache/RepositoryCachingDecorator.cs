﻿using SharedCore;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Core.Interfaces.Cache
{
    public class RepositoryCachingDecorator<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly Object LockObject = new();
        protected readonly string EntityName = typeof(TEntity).Name;
        protected readonly IGenericRepository<TEntity> InnerRepository;
        protected readonly ICacheService CacheService;

        public RepositoryCachingDecorator(IGenericRepository<TEntity> innerRepository, ICacheService cacheService)
        {
            InnerRepository = innerRepository;
            CacheService = cacheService;
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            var result = await InnerRepository.AddAsync(entity);
            lock (LockObject)
            {
                // Clear cached object collection
                CacheService.Remove(GetCacheCollectionKey());
            }

            return result;
        }

        public async Task<int> DeleteAsync(Guid id)
        {
            var result = await InnerRepository.DeleteAsync(id);
            lock (LockObject)
            {
                var key = GetCacheObjectKey(id);
                CacheService.Remove(key);
                // Clear cached object collection
                CacheService.Remove(GetCacheCollectionKey());
            }

            return result;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            var key = GetCacheCollectionKey();
            if (CacheService.TryGet<IReadOnlyList<TEntity>>(key, out var entities))
            {
                var searchKey = $"{EntityName}_Id";
                var relatedKeys = CacheService.FilterKeys(searchKey);
                var exceptItems = entities.Select(x => GetCacheObjectKey(x.Id))
                    .Except(relatedKeys);

                if (!exceptItems.Any())
                    return entities;
            }

            entities = await InnerRepository.GetAllAsync();
            lock (LockObject)
            {
                // cached object collection
                CacheService.Set(key, entities);
                // cached individual item of collection
                foreach (var item in entities)
                {
                    var itemKey = GetCacheObjectKey(item.Id);
                    CacheService.Set(itemKey, item);
                }
            }

            return entities;
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            var key = GetCacheObjectKey(id);
            if (!CacheService.TryGet<TEntity>(key, out var entity))
            {
                entity = await InnerRepository.GetByIdAsync(id);
                lock (LockObject)
                {
                    CacheService.Set(key, entity);
                }
            }

            return entity;
        }

        protected string GetCacheCollectionKey() => $"{EntityName}_List";
        protected string GetCacheObjectKey(Guid id) => $"{EntityName}_Id_{id}";
    }
}
