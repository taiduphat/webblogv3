﻿using SharedCore;
using TemplateService.Core.Dtos;

namespace TemplateService.Core.Interfaces
{
    public interface ITemplateService
    {
        Task<ServiceResult<Guid>> Insert(TemplateDto dto);
        Task<ServiceResult<Guid>> Update(TemplateDto dto);
        Task<ServiceResult<List<TemplateDto>>> GetTemplates();
        Task<ServiceResult<TemplateDto>> GetTemplateById(Guid id);
        Task<ServiceResult<bool>> Remove(Guid id);
        Task<ServiceResult<List<FieldTypeDto>>> GetFieldTypes();
    }
}
