﻿using System.ComponentModel;

namespace TemplateService.Core.Enums
{
    public enum FieldType
    {
        [Description("Yes/no switch used for filtering data that’s displayed in your site")]
        [DefaultValue(Constants.BoolId)]
        Bool,

        [Description("Date field used to display any combination of month, day, year, and time")]
        [DefaultValue(Constants.DateTimeId)]
        DateTime,

        [Description("Field containing item referenced from another Collection")]
        [DefaultValue(Constants.ItemRefId)]
        ItemRef,

        [Description("Field containing items referenced from another Collection")]
        [DefaultValue(Constants.ItemSetRefId)]
        ItemSetRef,

        [Description("URL field where the value can be used as a link")]
        [DefaultValue(Constants.LinkId)]
        Link,

        [Description("Single line input field used only for numbers")]
        [DefaultValue(Constants.NumberId)]
        Number,

        [Description("Unformatted text")]
        [DefaultValue(Constants.PlainTextId)]
        PlainText,

        [Description("Formatted text")]
        [DefaultValue(Constants.RichTextId)]
        RichText,

        [Description("Field containing file referenced from another Collection")]
        [DefaultValue(Constants.FileRefId)]
        FileRef
    }
}
