﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SharedCore;
using System.Diagnostics;
using TemplateService.Core.Dtos;
using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Core.Services
{
    public class TemplateService : ITemplateService
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;

        public TemplateService(IDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<ServiceResult<TemplateDto>> GetTemplateById(Guid id)
        {
            var template = await _dbContext.Templates.GetByIdAsync(id);
            if (template == null)
                return ServiceResult<TemplateDto>.Fail(StatusCode.TemplateNotFound);

            var templateFields = await _dbContext.Fields.GetFieldsByTemplateIdAsync(id);

            var templateDto = _mapper.Map<TemplateDto>(template);
            templateDto.Fields = _mapper.Map<IList<FieldDto>>(templateFields);

            var result = ServiceResult<TemplateDto>.Success(templateDto);
            return result;
        }

        public async Task<ServiceResult<List<TemplateDto>>> GetTemplates()
        {
            var templates = await _dbContext.Templates.GetAllAsync();
            var result = ServiceResult<List<TemplateDto>>.Success(_mapper.Map<List<TemplateDto>>(templates));

            return result;
        }

        public async Task<ServiceResult<Guid>> Insert(TemplateDto dto)
        {
            try
            {
                await _dbContext.BeginTransactionAsync();

                var isExist = await IsFieldTypeExisted(dto.Fields);
                if (!isExist)
                    return ServiceResult<Guid>.Fail(StatusCode.FieldTypeNotFound);

                var templateEntity = _mapper.Map<Template>(dto);
                templateEntity.Id = Guid.NewGuid();
                templateEntity.CreatedOn = DateTime.UtcNow;

                await _dbContext.Templates.AddAsync(templateEntity);

                foreach (var item in dto.Fields)
                {
                    item.Id ??= Guid.NewGuid();
                    item.CreatedOn = DateTime.UtcNow;
                    item.TemplateId = templateEntity.Id;
                    var field = _mapper.Map<Field>(item);

                    await _dbContext.Fields.AddAsync(field);
                }

                await _dbContext.CommitAsync();

                return ServiceResult<Guid>.Success(templateEntity.Id);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                await _dbContext.RollbackAsync();
                return ServiceResult<Guid>.Fail(StatusCode.InternalServerError);
            }
        }

        public async Task<ServiceResult<bool>> Remove(Guid id)
        {
            try
            {
                await _dbContext.BeginTransactionAsync();

                var template = await _dbContext.Templates.GetByIdAsync(id);
                if (template == null)
                    return ServiceResult<bool>.Fail(StatusCode.TemplateNotFound);

                var fields = await _dbContext.Fields.GetFieldsByTemplateIdAsync(id);
                await _dbContext.Fields.DeletesAsync(fields.Select(x => x.Id));
                await _dbContext.Templates.DeleteAsync(id);

                await _dbContext.CommitAsync();
                
                return ServiceResult<bool>.Success(true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                await _dbContext.RollbackAsync();
                return ServiceResult<bool>.Fail(StatusCode.InternalServerError);
            }
        }

        public async Task<ServiceResult<Guid>> Update(TemplateDto templateDto)
        {
            ArgumentNullException.ThrowIfNull(templateDto?.Id);

            var beforeEdit = await _dbContext.Templates.GetByIdAsync(templateDto.Id.Value);
            if (beforeEdit == null)
                return ServiceResult<Guid>.Fail(StatusCode.TemplateNotFound);

            var dbFields = await _dbContext.Fields.GetFieldsByTemplateIdAsync(templateDto.Id.Value);

            // updated fields should be template's fields
            var fieldIds = templateDto.Fields
                .Where(x => x.Id.HasValue)
                .Select(x => x.Id.GetValueOrDefault())
                .ToArray();
            var isExisted = await AreFieldsExisted(templateDto.Id.Value, fieldIds);
            if (!isExisted)
                return ServiceResult<Guid>.Fail(StatusCode.FieldNotFound);

            // updated field type should be existed
            isExisted = await IsFieldTypeExisted(templateDto.Fields);
            if (!isExisted)
                return ServiceResult<Guid>.Fail(StatusCode.FieldTypeNotFound);

            try
            {
                await _dbContext.BeginTransactionAsync();

                beforeEdit.UpdatedOn = DateTime.UtcNow;
                beforeEdit.Name = templateDto.Name;
                beforeEdit.Slug = templateDto.Slug;

                // update item
                await _dbContext.Templates.UpdateAsync(beforeEdit);

                // update existed item values
                var joinResult = from dbField in dbFields
                                 join field in templateDto.Fields on dbField.Id equals field.Id
                                 select (dbField, field);
                foreach (var (dbField, field) in joinResult)
                {
                    dbField.FieldTypeId = field.FieldTypeId;
                    dbField.Name = field.Name;
                    dbField.UpdatedOn = DateTime.UtcNow;
                    dbField.IsRequired = field.IsRequired;
                    dbField.IsEditable = field.IsEditable;
                    dbField.DataSource = field.DataSource;

                    await _dbContext.Fields.UpdateAsync(dbField);
                }

                // remove item
                var removeEntities = dbFields
                    .Where(x => !fieldIds.Contains(x.Id));

                await _dbContext.Fields.DeletesAsync(removeEntities.Select(x => x.Id));

                // add new item values
                var addFields = templateDto.Fields
                    .Where(x => !x.Id.HasValue)
                    .Select(item => new Field
                    {
                        Id = Guid.NewGuid(),
                        FieldTypeId = item.FieldTypeId,
                        Name = item.Name,
                        CreatedOn = DateTime.UtcNow,
                        IsRequired = item.IsRequired,
                        DataSource = item.DataSource,
                        TemplateId = beforeEdit.Id
                    });
                foreach (var field in addFields)
                {
                    await _dbContext.Fields.AddAsync(field);
                }

                await _dbContext.CommitAsync();

                return ServiceResult<Guid>.Success(templateDto.Id.Value);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                await _dbContext.RollbackAsync();
                return ServiceResult<Guid>.Fail(StatusCode.InternalServerError);
            }
        }

        public async Task<ServiceResult<List<FieldTypeDto>>> GetFieldTypes()
        {
            var fieldTypes = await _dbContext.FieldTypes.GetAllAsync();
            var result = _mapper.Map<List<FieldTypeDto>>(fieldTypes);

            return ServiceResult<List<FieldTypeDto>>.Success(result);
        }

        protected async Task<bool> IsFieldTypeExisted(IList<FieldDto> fieldDtos)
        {
            var fieldTypeIds = (await _dbContext.FieldTypes.GetAllAsync())
                .Select(x => x.Id);
            var isExisted = !fieldDtos.Any(x => !fieldTypeIds.Contains(x.FieldTypeId));

            return isExisted;
        }

        protected async Task<bool> AreFieldsExisted(Guid templateId, Guid[] fieldIds)
        {
            var dbFields = await _dbContext.Fields.GetFieldsByTemplateIdAsync(templateId);
            var dbFieldIds = dbFields.Select(x => x.Id);

            var excepts = !fieldIds.Except(dbFieldIds).Any();

            return excepts;
        }
    }
}
