﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces;
using TemplateService.Core.Interfaces.Cache;
using TemplateService.Core.Interfaces.Repository;
using TemplateService.Infrastructure.Cache;
using TemplateService.Infrastructure.Handlers;
using TemplateService.Infrastructure.Persistence;

namespace TemplateService
{
    public static class DependencyInjection
    {
        public static void AddTemplateService(this IServiceCollection services, IConfiguration configuration)
        {
            // Core services
            services.AddScoped<ITemplateService, Core.Services.TemplateService>();
            services.AddScoped<IDbConnection>(_ => new SqlConnection(configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<FieldRepository>();
            services.AddScoped<TemplateRepository>();
            services.AddScoped<GenericRepositoryBase<FieldType>>();
            services.AddScoped<ICacheService, MemoryCacheService>();
            
            services.AddScoped<IFieldRepository>(provider => new FieldCachingRepositoryDecorator(
                provider.GetRequiredService<FieldRepository>(), 
                provider.GetRequiredService<ICacheService>()));

            services.AddScoped<ITemplateRepository>(provider => new TemplateCachingRepositoryDecorator(
                provider.GetRequiredService<TemplateRepository>(),
                provider.GetRequiredService<ICacheService>()));
            
            services.AddScoped<IGenericRepository<FieldType>>(provider => new RepositoryCachingDecorator<FieldType>(
                provider.GetRequiredService<GenericRepositoryBase<FieldType>>(),
                provider.GetRequiredService<ICacheService>()));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IDbContext, DbContext>();

            // DateTime in database is formatted as utc
            SqlMapper.AddTypeHandler(new DateTimeHandler());
        }
    }
}
