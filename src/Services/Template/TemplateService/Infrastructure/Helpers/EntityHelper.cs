﻿namespace TemplateService.Infrastructure.Helpers
{
    public static class EntityHelper<TEntity> where TEntity : class
    {
        public static string[] GetColumns()
        {
            var entityType = typeof(TEntity);
            var properties = entityType.GetProperties(
                System.Reflection.BindingFlags.Public | 
                System.Reflection.BindingFlags.Instance);

            return properties.Select(p => p.Name).ToArray();
        }

        public static string GetTableName()
        {
            return typeof(TEntity).Name;
        }
    }
}
