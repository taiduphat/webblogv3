﻿using System.Collections.Concurrent;
using TemplateService.Core.Interfaces.Cache;

namespace TemplateService.Infrastructure.Cache
{
    internal class MemoryCacheService : ICacheService
    {
        private static readonly ConcurrentDictionary<string, object> _objectStorage = new();

        public MemoryCacheService()
        {
        }

        public string[] FilterKeys(string searchKey)
        {
            var filteredKeys = _objectStorage.Keys
                .Where(key => key.StartsWith(searchKey));

            return filteredKeys.ToArray();
        }

        public void Remove(string cacheKey)
        {
            _objectStorage.TryRemove(cacheKey, out _);
        }

        public T Set<T>(string cacheKey, T value)
        {
            ArgumentNullException.ThrowIfNull(cacheKey);
            ArgumentNullException.ThrowIfNull(value);

            _objectStorage.TryAdd(cacheKey, value);

            return value;
        }

        public bool TryGet<T>(string cacheKey, out T value)
        {
            ArgumentNullException.ThrowIfNull(cacheKey);

            var result = _objectStorage.TryGetValue(cacheKey, out var valueObject);
            if (result && valueObject != null)
                value = (T)valueObject;
            else
                value = default(T);
            
            return result;
        }
    }
}
