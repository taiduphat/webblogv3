﻿using Dapper;
using System.Data;

namespace TemplateService.Infrastructure.Handlers
{
    /// <summary>
    /// The DateTime value was stored in UTC but when getting its not in UTC format.
    /// </summary>
    public class DateTimeHandler : SqlMapper.TypeHandler<DateTime>
    {
        public override DateTime Parse(object value) 
            => DateTime.SpecifyKind((DateTime)value, DateTimeKind.Utc);

        public override void SetValue(IDbDataParameter parameter, DateTime value)
        {
            parameter.Value = value;
        }
    }
}
