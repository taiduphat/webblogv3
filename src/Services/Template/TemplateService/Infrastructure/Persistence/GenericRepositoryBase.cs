﻿using Dapper;
using SharedCore;
using System.Data;
using TemplateService.Core.Interfaces.Repository;
using TemplateService.Infrastructure.Helpers;

namespace TemplateService.Infrastructure.Persistence
{
    public class GenericRepositoryBase<TEntity> : IGenericRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IUnitOfWork _unitOfWork;
        protected IDbConnection Connection => _unitOfWork.Connection;
        protected IDbTransaction? Transaction => _unitOfWork.Transaction;

        protected string TableName => EntityHelper<TEntity>.GetTableName();

        public GenericRepositoryBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> AddAsync(TEntity entity)
        {
            var entityColumns = EntityHelper<TEntity>.GetColumns();

            var sql = string.Format("INSERT INTO {0} ({1}) VALUES ({2});",
                TableName, string.Join(",", entityColumns), string.Join(",", entityColumns.Select(x => $"@{x}")));

            var result = await Connection.ExecuteAsync(sql, entity, Transaction);
            return result;
        }

        public async Task<int> DeleteAsync(Guid id)
        {
            var sql = $"DELETE FROM {TableName} WHERE {nameof(BaseEntity.Id)} = @Id";
            var result = await Connection.ExecuteAsync(sql, new { Id = id }, Transaction);
            return result;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            var sql = $"SELECT * FROM {TableName}";
            var result = await Connection.QueryAsync<TEntity>(sql, null, Transaction);
            return result.ToList();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            var sql = $"SELECT * FROM {TableName} WHERE {nameof(BaseEntity.Id)} = @Id";
            var entity = await Connection.QueryFirstOrDefaultAsync<TEntity>(sql, new { Id = id }, Transaction);
            return entity;
        }
    }
}
