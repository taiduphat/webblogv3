﻿using Dapper;
using System.Data;
using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Infrastructure.Persistence
{
    public class TemplateRepository : GenericRepositoryBase<Template>, ITemplateRepository
    {
        public TemplateRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public async Task<int> UpdateAsync(Template template)
        {
            var sql = $@"UPDATE {TableName}
                        SET Name=@Name, 
                            Slug=@Slug,
                            UpdatedOn=@UpdatedOn
                        WHERE Id=@Id";

            var result = await Connection.ExecuteAsync(sql, template, Transaction);
            return result;
        }
    }
}
