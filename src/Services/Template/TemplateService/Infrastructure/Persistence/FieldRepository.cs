﻿using Dapper;
using System.Data;
using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Infrastructure.Persistence
{
    public class FieldRepository : GenericRepositoryBase<Field>, IFieldRepository
    {
        public FieldRepository(IUnitOfWork unitOfWork) 
            : base(unitOfWork)
        {
        }

        public async Task<int> DeletesAsync(IEnumerable<Guid> ids)
        {
            var sql = $"DELETE FROM {TableName} WHERE {nameof(Field.Id)} IN @Ids";
            var result = await Connection.ExecuteAsync(sql, new { Ids = ids.ToArray() }, Transaction);
            return result;
        }

        public async Task<List<Field>> GetFieldsByTemplateIdAsync(Guid templateId)
        {
            var templateIdField = nameof(Field.TemplateId);

            var sql = $"SELECT * FROM {TableName}" +
                      $" WHERE {templateIdField} = @{templateIdField}";

            var queryParams = new DynamicParameters();
            queryParams.Add(templateIdField, templateId);

            var result = await Connection.QueryAsync<Field>(sql, queryParams);
            return result.ToList();
        }

        public async Task<int> UpdateAsync(Field field)
        {
            var sql = $@"UPDATE {TableName}
                        SET Name=@Name, 
                            FieldTypeId=@FieldTypeId,
                            DataSource=@DataSource,
                            IsRequired=@IsRequired,
                            IsEditable=@IsEditable,
                            UpdatedOn=@UpdatedOn
                        WHERE Id=@Id";

            var result = await Connection.ExecuteAsync(sql, field, Transaction);
            return result;
        }
    }
}
