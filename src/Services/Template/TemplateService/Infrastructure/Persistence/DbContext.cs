﻿using SharedCore;
using TemplateService.Core.Entities;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Infrastructure.Persistence
{
    internal class DbContext : IDbContext
    {
        private readonly IUnitOfWork _unitOfWork;

        public DbContext(IUnitOfWork unitOfWork,
            ITemplateRepository templates,
            IFieldRepository fields,
            IGenericRepository<FieldType> fieldTypes)
        {
            _unitOfWork = unitOfWork;

            FieldTypes = fieldTypes;
            Fields = fields;
            Templates = templates;
        }

        public ITemplateRepository Templates { get; }
        public IFieldRepository Fields { get; }
        public IGenericRepository<FieldType> FieldTypes { get; }

        public Task BeginTransactionAsync() => _unitOfWork.BeginTransactionAsync();
        public Task CommitAsync() => _unitOfWork.CommitAsync();
        public Task RollbackAsync() => _unitOfWork.RollbackAsync();
    }
}
