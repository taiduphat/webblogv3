﻿using System.Data;
using System.Data.Common;
using TemplateService.Core.Interfaces.Repository;

namespace TemplateService.Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbConnection _connection;
        private DbTransaction? _transaction { get; set; }

        public UnitOfWork(IDbConnection dbConnection)
        {
            _connection = (DbConnection)dbConnection;
            if (_connection.State == ConnectionState.Closed)
                _connection.Open();
        }

        public IDbConnection Connection => _connection;
        public IDbTransaction? Transaction => _transaction;

        public async Task BeginTransactionAsync()
        {
            _transaction = await _connection.BeginTransactionAsync();
        }

        public async Task CommitAsync()
        {
            if (_transaction == null)
                throw new ArgumentNullException(nameof(_transaction));

            await _transaction.CommitAsync();
        }

        public void Dispose()
        {
            if (_transaction != null)
                _transaction.Dispose();

            _transaction = null;
            GC.SuppressFinalize(this);
        }

        public async Task RollbackAsync()
        {
            if (_transaction == null)
                throw new ArgumentNullException(nameof(_transaction));

            await _transaction.RollbackAsync();
        }
    }
}
