# WebBlogV3

Build webblog application as microservices:

- Template service
- Image service
- Item service
- Authentication service 

Studying notes: 
- UnitOfWork in Dapper to manage transaction
- Implement MemoryCaching
- Services comunicate each other
- Building Webblog in microservices
